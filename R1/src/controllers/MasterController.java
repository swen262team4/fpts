package controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import models.Bank;
import models.Holding;
import models.MoneyMarket;
import models.OwnedStock;
import models.Stock;
import models.Transaction;
import models.UndoRedo;
import models.User;


/*
 * Main frame for FPTS controllers.
 */
public abstract class MasterController {
    //NEW This will return any messages (error, notification) from a controller to the view
    protected String message;
	public void setMessage(String m){
	    this.message = m;
	}
	public String getMessage(){
	    return message;
	}

    public String[][] prepareHoldingsForTable(ArrayList<Holding> holdings){
    	if (holdings.size() == 0){
    		String[][] table = new String[1][6];
    		table[0][0] = "Portfolio is empty";
    		table[0][1] = "";
    		table[0][2] = "";
    		table[0][3] = "";
    		table[0][4] = "";
    		table[0][5] = "";
    		return table;
    	}
        int len = 0; //this is used to determine the length of each array.
        for (Holding holding : holdings){
            int size = holding.getPartOf().size();
            if (holding.getTickerSymbol().charAt(0) == '!')
                size--; //ticker symbol is not printed
            if (size > len){
                len = holding.getPartOf().size(); //update longest array size
            }
        }
        len += 3; //this is done because ticker symbol, name, and initValue are added to the partOf list
        int wid = holdings.size();
        String[][] table = new String[wid][len];
        Holding holding;
        for (int i = 0; i < wid; i++){
            holding = holdings.get(i);
            if (holding.getTickerSymbol().charAt(0) == '!'){
                if (holding.getTickerSymbol() == "!OWNED"){
                    OwnedStock stock = (OwnedStock)holding;
                    table[i][0] = stock.getTickerSymbolReal();
                    table[i][1] = stock.getNameReal();
                    table[i][2] = Double.toString(stock.getInitPriceReal());
                    table[i][3] = Double.toString(stock.getSharesOwned());
                    table[i][4] = stock.getDate();
                    int j;
                    for (j = 0; j < stock.getPartOfReal().size(); j++){
                        table[i][j+5] = stock.getPartOfReal().get(j);
                    }
                    j += 5;
                    for (;j < len; j++){
                        table[i][j] = "";
                    }
                }else if(holding.getTickerSymbol() == "!BANK"){
                    Bank bank = (Bank)holding;
                    table[i][1] = bank.getName();
                    table[i][2] = bank.getInitPriceString();
                    table[i][3] = Integer.toString(bank.getRoutingNumber());
                    table[i][4] = Integer.toString(bank.getAccountNumber());
                    table[i][5] = bank.getDate();
                    for (int j = 6; j < len; j++){
                        table[i][j] = "";
                    }
                }else if(holding.getTickerSymbol() == "!MM"){
                    MoneyMarket mm = (MoneyMarket)holding;
                    table[i][0] = mm.getName();
                    table[i][1] = mm.getInitPriceString();
                    table[i][2] = Integer.toString(mm.getRoutingNumber());
                    table[i][3] = Integer.toString(mm.getAccountNumber());
                    table[i][4] = mm.getDate();
                    for (int j = 5; j < len; j++){
                        table[i][j] = "";
                    }
                }else if(holding.getTickerSymbol() == "!WATCH"){
                	Stock stock = (Stock)holding;
                	table[i][1] = holding.getTickerSymbol();
                    table[i][2] = holding.getName();
                    table[i][3] = holding.getInitPriceString();
                    int j;
                    for (j = 0; j < holding.getPartOf().size(); j++){
                        table[i][j+4] = holding.getPartOf().get(j);
                    }
                    j += 4;
                    for (;j < len; j++){
                        table[i][j] = "";
                    }
                }
            }else{
                table[i][0] = holding.getTickerSymbol();
                table[i][1] = holding.getName();
                table[i][2] = holding.getInitPriceString();
                int j;
                for (j = 0; j < holding.getPartOf().size(); j++){
                    table[i][j+3] = holding.getPartOf().get(j);
                }
                j += 3;
                for (;j < len; j++){
                    table[i][j] = "";
                }
            }
        }
        return table;
    }
    
    public String[][] prepareWatchedHoldingsForTable(ArrayList<Stock> watchedStocks) {
    	if (watchedStocks.size() == 0){
    		String[][] table = new String[1][6];
    		table[0][0] = "Watch list is empty";
    		table[0][1] = "";
    		table[0][2] = "";
    		table[0][3] = "";
    		table[0][4] = "";
    		table[0][5] = "";
    		return table;
    	}
        int len = 0; //this is used to determine the length of each array.
        for (Stock stock : watchedStocks){
            int size = stock.getPartOf().size();
            if (stock.getTickerSymbol().charAt(0) == '!')
                size--; //ticker symbol is not printed
            if (size > len){
                len = stock.getPartOf().size(); //update longest array size
            }
        }
        len += 3; //this is done because ticker symbol, name, and initValue are added to the partOf list
        int wid = watchedStocks.size();
        String[][] table = new String[wid][len];
        Stock stock;
        for (int i = 0; i < wid; i++){
        	stock = watchedStocks.get(i);
            table[i][0] = stock.getTickerSymbol();
            table[i][1] = stock.getName();
            table[i][2] = stock.getInitPriceString();
            int j;
            for (j = 0; j < stock.getPartOf().size(); j++){
                table[i][j+3] = stock.getPartOf().get(j);
            }
            j += 3;
            for (;j < len; j++){
                table[i][j] = "";
            }
        }
        return table;
	}

    public String[][] prepareTransactionsForTable(ArrayList<Transaction> transactions){
    	if (transactions.size() == 0){
    		String[][] table = new String[1][6];
    		table[0][0] = "No Transactions Available";
    		table[0][1] = "";
    		table[0][2] = "";
    		table[0][3] = "";
    		table[0][4] = "";
    		table[0][5] = "";
    		return table;
    	}
    	int wid = transactions.size();
        String[][] table = new String[wid][6];
        Transaction transaction;
        for (int i = 0; i < wid; i++){
        	transaction = transactions.get(i);
            if(transaction.getTickerSymbol().equals("!BANK")){
                table[i][0] = "";
            }else{
            	table[i][0] = transaction.getTickerSymbol();
            }
            table[i][1] = transaction.getName();
            table[i][2] = Double.toString(transaction.getPrice());
            table[i][3] = Double.toString(transaction.getSharesPurchased());
            table[i][4] = transaction.getDate();
            if (transaction.getBankName().equals("!DEPOSIT")){
            	table[i][5] = "Deposited/Withdrawn";
            }else{
            	table[i][5] = transaction.getBankName();
            }
        }
        return table;
    }
}