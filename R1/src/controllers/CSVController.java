package controllers;

import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.StringTokenizer;

import models.Bank;
import models.CSV;
import models.Holding;
import models.MoneyMarket;
import models.OwnedStock;
import models.Stock;
import models.Transaction;
import models.User;
import models.WatchedStock;

/*
 * This class is control of CSV file
 */
public class CSVController extends MasterController {
	private CSV currentHoldings;

	/**
	 * This function initializes the view with the user's holdings.
	 *
	 * @author Max
	 * @param userID
	 * @return user's portfolio
	 */

	public void init(){
		currentHoldings = new CSV();
		getPortfolio();
	}

	public void getPortfolio(){
		try{
			currentHoldings.setHolding(User.getInstance().getPortfolio().getHoldings());
			currentHoldings.setTransactions(User.getInstance().getTransactionList());
		}catch (Exception e){
			System.out.println("NO PORTFOLIO");
			e.printStackTrace();
		}
	}

	public String[][] getTable(){
		return prepareHoldingsForTable(this.currentHoldings.getHoldingList());
	}
	

	
	
	public String[][] getTransactionTable(){
		return prepareTransactionsForTable(this.currentHoldings.getTransactions());
	}

	public ArrayList<Holding> getHoldingList(){
		CSV csv = openCSV("equities.csv");
		return csv.getHoldingList();
	}
	
	/** openCSV takes a filepath and tokenizes the csv file located there.
	 *
	 * @author Max
	 * @param filename - String.  Path to the csv file.
	 * @return holdings that were in the csv
	 */
	public CSV openCSV(String filename){
		CSV csv = new CSV();
		try {
			String[] st = null;
			FileReader inputFileStream = new FileReader(filename);
			BufferedReader inputStream = new BufferedReader(inputFileStream);
			String tickerSymbol, name, line;
			Double value;
			//NEW
			double highTriggerValue;
			double lowTriggerValue;
			String status;

			ArrayList<String> partOf;
			while ((line = inputStream.readLine()) != null){
				st = line.split("\",\"");
				//.replaceAll("^\"+|\"+$", "") is a regex to remove the leading and training " marks
				tickerSymbol = st[0].replaceAll("^\"+|\"+$", "");
				if (tickerSymbol.charAt(0) == '!'){
					if (tickerSymbol.equals("!BANK")){
						Bank bank = new Bank(st[1], Double.parseDouble(st[2]), Integer.parseInt(st[3]), Integer.parseInt(st[4]), st[5].replaceAll("^\"+|\"+$", ""));
						csv.addHolding((Holding)bank);
					}else if(tickerSymbol.equals("!T")){
						Transaction trans = new Transaction(st[1], st[2], Double.parseDouble(st[3]), Double.parseDouble(st[4]), st[5], st[6].replaceAll("^\"+|\"+$", ""));
						csv.addTransaction(trans);
					}else if(tickerSymbol.equals("!OWNED")){
						partOf = new ArrayList<String>();
						for (int i = 6; i < st.length; i++){
							partOf.add(st[i].replaceAll("^\"+|\"+$", ""));
						}
						OwnedStock stock = new OwnedStock(st[1], st[2], Double.parseDouble(st[3]), Double.parseDouble(st[4]), st[5], partOf);
						csv.addHolding(stock);
					}else if(tickerSymbol.equals("!MM")){
						MoneyMarket mm = new MoneyMarket(st[1], Double.parseDouble(st[2]), Integer.parseInt(st[3]), Integer.parseInt(st[4]), st[5].replaceAll("^\"+|\"+$", ""));
						csv.addHolding((Holding)mm);
					}else if(tickerSymbol.equals("!WATCH")){
						tickerSymbol = st[1].replaceAll("^\"+|\"+$", "");
						name = st[2].replaceAll("^\"+|\"+$", "");
						value = Double.parseDouble(st[3].replaceAll("^\"+|\"+$", ""));
						//NEW
						highTriggerValue = Double.parseDouble(st[4].replaceAll("^\"+|\"+$", ""));
						lowTriggerValue = Double.parseDouble(st[5].replaceAll("^\"+|\"+$", ""));
						status = st[6].replaceAll("^\"+|\"+$", "");
						
						partOf = new ArrayList<String>();
						
						for (int i = 7; i < st.length; i++){
							partOf.add(st[i].replaceAll("^\"+|\"+$", ""));
						}
						csv.addWatch(new WatchedStock(tickerSymbol, name, value, partOf, highTriggerValue,lowTriggerValue, status));
					}
				}else{
					name = st[1].replaceAll("^\"+|\"+$", "");
					value = Double.parseDouble(st[2].replaceAll("^\"+|\"+$", ""));
					partOf = new ArrayList<String>();
					for (int i = 3; i < st.length; i++){
						partOf.add(st[i].replaceAll("^\"+|\"+$", ""));
					}
					csv.addHolding(new Stock(tickerSymbol, name, value, partOf));
				}
			}
			inputStream.close();
			currentHoldings = csv;
		}catch (FileNotFoundException e){
			System.out.println("File Not found.  Please try again and make sure the path is correct");
		}catch (Exception e){
			System.out.println("An unknown error occurred.");
			e.printStackTrace();
			System.exit(0);
		}
		return csv;
	}

	public void createCSV(CSV csv, String filepath){
		this.currentHoldings = csv;
		createCSV(filepath);
	}

	public void createCSV(String filepath){
		//A string builder was used because it is more efficient for long string concatonation
		StringBuilder builder = new StringBuilder();

		for (Holding holding : currentHoldings.getHoldingList()){
			//The csv file has quotes around all of the strings, so they are added here.
			builder.append("\"" + holding.getTickerSymbol() +
					"\",\"" + holding.getName() +
					"\",\"" + holding.getInitPriceString() + "\"");
			for (String part : holding.getPartOf()){
				builder.append(",\"" + part + "\"");
			}
			builder.append("\n");
		}

		for (Transaction trans : currentHoldings.getTransactions()){
		    //NEW* Add "!T"
			builder.append("\"" + trans.getTranMark() + ("\",")+
			        "\"" + trans.getTickerSymbol() +
					"\",\"" + trans.getName() +
					"\",\"" + trans.getPrice() +
					"\",\"" + trans.getSharesPurchased() +
					"\",\"" + trans.getDate() +
					"\",\"" + trans.getBankName() + "\"\n");
		}

		for (WatchedStock stock : currentHoldings.getWatch()){
			//The csv file has quotes around all of the strings, so they are added here.
			builder.append("\"!WATCH" +
					"\",\"" + stock.getTickerSymbolReal() +
					"\",\"" + stock.getNameReal() +
					"\",\"" + stock.getInitPrice() + "\""+
					"\",\"" + stock.getHighTriggerPoint() +"\""+
					"\",\"" + stock.getLowTriggerPoint() +"\""+
					"\",\"" + stock.getStatus() +"\"");
			
			
			for (String part : stock.getPartOfReal()){
				builder.append(",\"" + part + "\"");
			}
			builder.append("\n");
		}
		
		Path path = Paths.get(filepath);
		try {
			if (Files.exists(path)){
				Files.write(path, builder.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
			}else{
				Files.write(path, builder.toString().getBytes(), StandardOpenOption.CREATE_NEW);
			}
		} catch (Exception e) {
			System.out.println("An unknown error occurred when trying to export the csv file.");
			e.printStackTrace();
		}
	}

	public void addCSVToPortfolio(){
		User user = User.getInstance();
		CSV csv = currentHoldings;
		user.addTransaction(csv.getTransactions());
		user.getPortfolio().addHolding(csv.getHoldingList());
		user.getPortfolio().addWatchlistHolding(csv.getWatch());
	}
	
	public void savePortfolio(){
		User user = User.getInstance();
		try{
			if (currentHoldings == null){
				currentHoldings = new CSV();
			}
			currentHoldings.setHolding(user.getPortfolio().getHoldings());
			currentHoldings.setTransactions(user.getTransactionList());
			currentHoldings.setWatch(user.getPortfolio().getWatchedHoldings());
			
			
			String filepath = user.getUserCSV();
			
			            
			createCSV(filepath);
		}catch (Exception e){
			System.out.println("NO PORTFOLIO");
			e.printStackTrace();
		}
	}
}