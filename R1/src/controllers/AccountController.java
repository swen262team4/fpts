package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import models.User;

/*
 * This class is control of user.
 */
public class AccountController extends MasterController
{


    //FPTS users
    private HashMap<String,User> users = new HashMap<String,User>();
    //**May use different data structure for logs
    //private ArrayList<String> accountLog;

    String message;


    public User createUser(String uName, String pWord)
    /**
     * Adds a new user to the system and returns the user singleton
     * consisting of that new user.
     *
     * @param uName     Username string
     * @param pWord     Password string
     * @return          New user object, null if an error occurred
     */
    {
        // This should create a user folder on the filesystem
        // as well as an empty .csv of their holdings
        
        //for pc
        String fileName = ".\\users\\" + uName + ".csv";
        String passFile = ".\\users\\" + uName + ".txt";
        
        //*for mac
        //String fileName = "./users/" + uName + ".csv";
        //String passFile = "./users/" + uName + ".txt";

        File csvFile = new File(fileName);
        PrintWriter writer, passWriter;

        // This will create the users directory if it doesn't
        // exist already
        if (!csvFile.getParentFile().exists())
        {
            csvFile.getParentFile().mkdirs();
        }
        
            try
            {
                // Create the new .csv
                writer = new PrintWriter(fileName, "UTF-8");
                passWriter = new PrintWriter(passFile, "UTF-8");

            //writer.println(pWord); // first line is password

            passWriter.println(encryptPassword(pWord));
            // txt file will contain PW

            writer.close();
            passWriter.close();

            User user = User.getInstance();

            String encryptedPass = encryptPassword(pWord);

            user.setID(uName);
                
            user.setPassword(encryptedPass);
                
            user.setUserCSV(fileName);

            this.message = uName + " CREATED.";
            this.setMessage(message);
            return user;


            } catch (Exception e) {
                System.err.println("Error creating new user file");
                System.err.println(e.getMessage());
            }
            return null;

    }


    /**
     * Log in using a specified username and password.
     * If the user does not exist, a new user will be created.
     * If the credentials are incorrect, return null.
     *
     * @param uName     Username string
     * @param pWord     Password string
     * @return          User singleton if successful, null if not
     */
    public User logIn(String uName, String pWord)
    {
        try
        {
            // Go to the user directory and get their csv
            //pc
            String userPath = "users\\" + uName + ".csv";
            String passPath = "users\\" + uName + ".txt";

            //mac
            //String userPath = "users/" + uName + ".csv";
            //String passPath = "users/" + uName + ".txt";

            //FileReader inputFileStream = new FileReader(userPath);
            FileReader inputFileStream = new FileReader(passPath);
            BufferedReader inputStream = new BufferedReader(inputFileStream);

            //String[] st = null;
            String line;

            line = inputStream.readLine();
            //st = line.split("\",\"");

            //if (st[0].equals(uName) && st[1].equals(pWord))
            // Password should ideally be encrypted in the future
            if (decryptPassword(line).equals(pWord))
            {
                User user = User.getInstance();

                user.setID(uName);
                user.setPassword(pWord);

                CSVController csvController = new CSVController();
                csvController.openCSV(userPath);
                csvController.addCSVToPortfolio();
                
                inputStream.close();
                
                user.setUserCSV(userPath);

                
                
                return user;
            } else {
                //incorrect password
                message = "THE PASSWORD YOU ENTERED IS INCORRECT";
                this.setMessage(message);
                inputStream.close();
                return null;
            }

        } catch (FileNotFoundException e) {

            message = "USER NOT FOUND. PLEASE REGISTER";
            this.setMessage(message);

            System.err.println("User file not found: creating user.");
            return createUser(uName, pWord);
        } catch (IOException e) {
            System.err.println("IOException in login");
        }

        // associate the current User object with the specified
        // user- if the information is incorrect, return null

        return null;
    }

    /**
     * Logs the current user out, disassociating the user singleton
     * from that user. This should return the program to the
     * default, non-logged-in state.
     *
     * @return      True if successful, false otherwise
     */
    public boolean logOut()
    {
        // log out, disassociate the user singleton with the
        // previously logged-in user, and return the program
        // to its initial state

        User currentUser = User.getInstance();

        currentUser = null;

        return true;
    }

    /**
     * Delete the specified user, if they exist. This is done by
     * removing their .csv file and folder. If the user does not
     * exist, this returns false.
     *
     * @param uName     Username
     * @return          True if user was removed, false otherwise
     */
    public static boolean deleteUser(String uName)
    {
        try
        {
            File userFile = new File("users\\" + uName + ".csv");
            File passFile = new File("users\\" + uName + ".txt");
            File dir = new File("users\\" + uName);

            userFile.delete();
            passFile.delete();
            dir.delete();

            return true;
        } catch (Exception e) {
            System.err.println("Error in deleting user");
        }

        return false;
    }

    /**
     * Encrypt the given string using a simple substitution
     * cipher.
     *
     * @param pw        Password
     * @return          Encrypted password
     */
    public String encryptPassword(String pw)
    {
        char[] charPw = pw.toCharArray();
        for (int i = 0; i < charPw.length; i++)
        {
            charPw[i] = (char) (charPw[i] + 4);
        }

        return String.valueOf(charPw);
    }

    /**
     * Decrypt the given string using a simple substitution
     * cipher.
     *
     * @param cipherPw  Encrypted password
     * @return          Password
     */
    public String decryptPassword(String cipherPw)
    {
        char[] charPw = cipherPw.toCharArray();
        for (int i = 0; i < charPw.length; i++)
        {
            charPw[i] = (char) (charPw[i] - 4);
        }

        return String.valueOf(charPw);
    }
}
