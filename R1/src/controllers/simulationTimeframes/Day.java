package controllers.simulationTimeframes;

public class Day extends Timeframe{
	private int stepsPerYear;
	
	public Day(){
		stepsPerYear = 365;
	}
	
	public int getStepsPerYear(){
		return stepsPerYear;
	}
}
