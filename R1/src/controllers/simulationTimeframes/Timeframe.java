package controllers.simulationTimeframes;

public abstract class Timeframe {
	private int stepsPerYear;
	public static Timeframe instance;
	public static Timeframe getInitializedTimeframe(){
		if (instance == null){
			instance = (Timeframe)new Day();
		}
		return instance;
	}
	
	public static void Instantiate(controllers.SimulationController.stepSize size){
		switch (size){
		case DAY:
			instance = (Timeframe)new Day();
			break;
		case MONTH:
			instance = (Timeframe)new Month();
			break;
		case YEAR: 
			instance = (Timeframe)new Year();
			break;
		case WEEK: 
			instance = (Timeframe)new Week();
			break;
		}
	}
	
	public int getStepsPerYear(){
		return stepsPerYear;
	}
}
