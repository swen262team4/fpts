package controllers.simulationTimeframes;

public class Week extends Timeframe{
	private int stepsPerYear;
	
	public Week(){
		stepsPerYear = 52;
	}
	
	public int getStepsPerYear(){
		return stepsPerYear;
	}
}
