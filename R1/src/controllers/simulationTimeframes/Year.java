package controllers.simulationTimeframes;

public class Year extends Timeframe{
	private int stepsPerYear;
	
	public Year(){
		stepsPerYear = 1;
	}
	
	public int getStepsPerYear(){
		return stepsPerYear;
	}
}
