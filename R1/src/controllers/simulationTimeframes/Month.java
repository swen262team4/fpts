package controllers.simulationTimeframes;

public class Month extends Timeframe{
	private int stepsPerYear;
	
	public Month(){
		stepsPerYear = 12;
	}
	
	public int getStepsPerYear(){
		return stepsPerYear;
	}
}
