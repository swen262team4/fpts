package controllers;

import java.util.ArrayList;
import java.util.HashMap;

import controllers.simulationAlgorithms.Algorithm;
import controllers.simulationTimeframes.Timeframe;
import models.Bank;
import models.Holding;
import models.MoneyMarket;
import models.OwnedStock;
import models.Stock;
import models.User;
import models.WatchedStock;

public class SimulationController {
	public enum stepSize{DAY, MONTH, YEAR, WEEK}
	public enum algorithmType{BEAR, BULL, NO_CHANGE}
	
	private ArrayList<Holding> holdings;
	private ArrayList<HashMap<String, Double> > simulation;
	private stepSize size;
	private algorithmType type;
	private int durration;
	private double percentAnnum;
	
	public SimulationController(){
		holdings = new ArrayList<Holding>();
		holdings.addAll(User.getInstance().getPortfolio().getHoldings());
		holdings.addAll(User.getInstance().getPortfolio().getWatchedHoldings());
		size = stepSize.DAY;
		type = algorithmType.NO_CHANGE;
		durration = 365;
		percentAnnum = 100;
		simulate(0);
	}

	public ArrayList<Holding> getHoldings() {
		return holdings;
	}

	public void setHoldings(ArrayList<Holding> holdings) {
		this.holdings = holdings;
	}
	
	public void addHoldings(Holding holding) {
		this.holdings.add(holding);
	}

	public stepSize getSize() {
		return size;
	}

	public void setSize(stepSize size) {
		this.size = size;
	}

	public int getDurration() {
		return durration;
	}

	public void setDurration(int durration) {
		this.durration = durration;
	}

	public double getPercentAnnum() {
		return percentAnnum;
	}

	public void setPercentAnnum(double percentAnnum) {
		this.percentAnnum = percentAnnum;
	}
	
	public algorithmType getType() {
		return type;
	}

	public void setType(algorithmType type) {
		this.type = type;
	}
	
	public ArrayList<HashMap<String, Double> > getSimulation(){
		return simulation;
	}
	
	public ArrayList<HashMap<String, Double> > getSimulation(int index){
		simulate(index);
		return simulation;
	}
	
	public void simulate(int index){
		Timeframe.Instantiate(size);
		Algorithm.Instantiate(type);
		if (simulation != null){
			for (Holding h : holdings){
				if (h.getTickerSymbol() == "!OWNED"){
	                OwnedStock stock = (OwnedStock)h;
	                if (simulation.size() > index && simulation.get(index).containsKey(stock.getTickerSymbolReal())){
	                	h.setInitPrice(simulation.get(index).get(stock.getTickerSymbolReal()));
	                }
	            }else if (h.getTickerSymbol().equals("!BANK")){
					continue;
				}else if (h.getTickerSymbol().equals("!MM")){
					continue;
	            }else{
	            	WatchedStock stock = (WatchedStock)h;
	                if (simulation.size() > index && simulation.get(index).containsKey(stock.getTickerSymbol())){
	                	h.setInitPrice(simulation.get(index).get(stock.getTickerSymbol()));
	                }
	            }
			}
		}
		simulation = Algorithm.getInitializedAlgorithm().getSimulation(holdings, percentAnnum, durration);
	}
	
	public String[][] prepareSimulationForTable(ArrayList<HashMap<String, Double> > simulation, int index){
    	ArrayList<Holding> holdings = new ArrayList<Holding>();
    	holdings.addAll(User.getInstance().getPortfolio().getHoldings());
    	holdings.addAll(User.getInstance().getPortfolio().getWatchedHoldings());
		if (holdings.size() == 0){
    		String[][] table = new String[1][6];
    		table[0][0] = "Portfolio is empty";
    		table[0][1] = "";
    		table[0][2] = "";
    		table[0][3] = "";
    		table[0][4] = "";
    		table[0][5] = "";
    		return table;
    	}
        int len = 0; //this is used to determine the length of each array.
        for (Holding holding : holdings){
            int size = holding.getPartOf().size();
            if (holding.getTickerSymbol().charAt(0) == '!')
                size--; //ticker symbol is not printed
            if (size > len){
                len = holding.getPartOf().size(); //update longest array size
            }
        }
        len += 3; //this is done because ticker symbol, name, and initValue are added to the partOf list
        int wid = holdings.size();
        String[][] table = new String[wid][len];
        Holding holding;
        for (int i = 0; i < wid; i++){
            holding = holdings.get(i);
            if (holding.getTickerSymbol() == "!OWNED"){
                OwnedStock stock = (OwnedStock)holding;
                table[i][0] = stock.getTickerSymbolReal();
                table[i][1] = stock.getNameReal();
                if (simulation != null && simulation.size() > index && simulation.get(index).containsKey(stock.getTickerSymbolReal())){
                	table[i][2] = Double.toString(simulation.get(index).get(stock.getTickerSymbolReal()));
                }else{
                	table[i][2] = Double.toString(stock.getInitPriceReal());
                }
                table[i][3] = Double.toString(stock.getSharesOwned());
                table[i][4] = stock.getDate();
                int j;
                for (j = 0; j < stock.getPartOfReal().size(); j++){
                    table[i][j+5] = stock.getPartOfReal().get(j);
                }
                j += 5;
                for (;j < len; j++){
                    table[i][j] = "";
                }
            }else if (holding.getTickerSymbol().equals("!BANK")){
				continue;
			}else if (holding.getTickerSymbol().equals("!MM")){
				continue;
            }else{
            	WatchedStock stock = (WatchedStock)holding;
                table[i][0] = stock.getTickerSymbolReal();
                table[i][1] = stock.getNameReal();
                if (simulation != null && simulation.size() > index && simulation.get(index).containsKey(stock.getTickerSymbol())){
                	table[i][2] = Double.toString(simulation.get(index).get(stock.getTickerSymbol()));
                }else{
                	table[i][2] = Double.toString(stock.getInitPrice());
                }
                int j;
                for (j = 0; j < stock.getPartOf().size(); j++){
                    table[i][j+3] = stock.getPartOf().get(j);
                }
                j += 3;
                for (;j < len; j++){
                    table[i][j] = "";
                }
            }
        }
        return table;
    }
}