package controllers;

import models.Holding;
import models.OwnedStock;
import models.Portfolio;
import models.Stock;
import models.Transaction;
import models.UndoRedo;
import models.User;
import models.WatchedStock;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import models.Bank;
import observer.Observer;
import observer.Subject;
import yahooAPI.YAHOO;
import models.Bank;
import models.CSV;


/*
 * This class is control of Portfolio
 */
public class PortfolioController extends MasterController implements Subject {
    
    
    
    public double DOWValue = 0;
    //timer for auto update. default 5s
    public static int time = 5000;
    private static Timer timer = new Timer();
    private AutoUpdate task = new AutoUpdate();
    
    
    /*
     * This will control the user portfolio. It handles all the 
     * action related to the portfolio. 
     * 
     */
    public PortfolioController(){
        
        this.observers = new ArrayList<Observer>();
        autoUpdate();
    }
    
    
    /*
     * Timer to trigger portfolio to update
     */
    public void autoUpdate(){
           
        PortfolioController.timer.schedule(task, 0, time);
                       
    }
    
    public void changeAutoUpdateTime(int time){
        PortfolioController.time = time;
        Timer temp = PortfolioController.timer;
        PortfolioController.timer.cancel();
        PortfolioController.timer = new Timer();
        this.task = new AutoUpdate();
        PortfolioController.timer.schedule(task, 0, time);
        
        
        
    }

    // add holding (in portfolio)
    // remove holding- arraylist remove that shifts remainders?
    // search and add from input CSV

    public void updateHoldings(Portfolio pf)
    {
        // this method should use the yahoo API
        // go through the user's holdings and use the ticker symbols
        // to construct the URL to send
        // send request, await reply, use results to update
        // price for all holdings
        // skip over any holdings that we have tagged with an "!"
        ArrayList<Holding> stocks = getOwnedStocks(pf);
        
        stocks.addAll(getWatchedStocks(pf));
        
        ArrayList<String> tickerList = new ArrayList<String>();
        for(Holding stock : stocks){
            if(stock instanceof OwnedStock){
                tickerList.add(((OwnedStock) stock).getTickerSymbolReal());
            }else{
                tickerList.add(((WatchedStock)stock).getTickerSymbolReal());
            }
        }

        YAHOO yahoo = new YAHOO();
        
        HashMap<String,Double> priceMap = new HashMap<String,Double>();
        try {
            DOWValue = yahoo.getDOW();
            if(tickerList.size()>0){
                priceMap = yahoo.getPrice(yahoo.buildURL(tickerList));
                

            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        
                
        for(Holding stock : stocks){
            if (stock instanceof OwnedStock){
                //System.out.println(stock.getTickerSymbolReal());
                //stock.setInitPrice(priceMap.get(stock.getTickerSymbol()));
                ((OwnedStock)stock).setInitPrice(priceMap.get(((OwnedStock) stock).getTickerSymbolReal()));
            }else if(stock instanceof WatchedStock){
                ((WatchedStock)stock).setInitPrice((priceMap.get(((WatchedStock)stock).getTickerSymbolReal())));
            }
        }
        notifyObserver();
    }
    
    //Views 
    private ArrayList<Observer> observers;
    


    /**
     * Add a holding to the portfolio's list.
     *
     * @param pf        Portfolio
     * @param hold      Holding to add
     * 
     */
    public void addHolding(Portfolio pf, Holding hold)
    {
        pf.addHolding(hold);
        notifyObserver();
    }
    

    
    /**
     * Add a holding to the portfolio's watchlist.
     * 
     * @param pf        Portfolio
     * @param hold      Holding to add
     */
    public void addWatchedHolding(Portfolio pf, WatchedStock hold)
    {
        pf.addWatchlistHolding(hold);

        notifyObserver();
    }
    
    /**
     * Remove a holding from the portfolio's watchlist.
     * 
     * @param pf        Portfolio
     * @param hold      Holding to remove
     */
    public void deleteWatchedHolding(Portfolio pf, WatchedStock hold)
    {
        pf.deleteWatchedHolding(hold);

        notifyObserver();
    }
   
    
    
    
    
    /**
     * 
     * Add new transaction to the user transaction list
     */
    public void addTransaction(Bank bank, Stock stock, int shares){
        //pf.addHolding(holding);
        String stockSymbol = stock.getTickerSymbol();
        String stockName = stock.getName();
        double price = stock.getInitPrice();
        double sharesPurchased = (double)shares;
        String bankName = bank.getName();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date dateObject = new Date();
        String date = dateFormat.format(dateObject);
        
        //Call master controller to record the transaction
        recordTransaction(stockSymbol, stockName, price,sharesPurchased,bankName);
        
        //NOW ADD THE OWNED STOCK TO THE PORTFOLIO
               
        Holding bought = new OwnedStock(stockSymbol, stockName, price, sharesPurchased, date, stock.getPartOf() );
        //addHolding(User.getInstance().getPortfolio(), bought);
        
        bankRemoveMoney(bank, price*shares);
        
        notifyObserver();

    }

    /**
     * Search a CSV for a specific holding by its ticker value,
     * and add it to the portfolio if it was found.
     *
     * @param pf            Portfolio
     * @param csvFile       CSV object to read
     * @param ticker        Ticker symbol to use for search
     * @return              True if holding found, false if not
     */
    public boolean searchAddHolding(Portfolio pf, CSV csvFile, String ticker)
    {
        ArrayList<Holding> list = csvFile.getHoldingList();
        Holding currentHold;

        for (int i = 0; i < list.size(); i++)
        {
            currentHold = list.get(i);
            if (currentHold.getTickerSymbol().equals(ticker))
            {
                pf.addHolding(currentHold);
                return true;
            }
        }

        return false;
    }
    
    /**
     * Searches through the CSV for an equity that contains the specified
     * ticker, name, or index strings. This uses the String contains()
     * method, so a partial match with the holding's value will
     * cause it to be added to the results arraylist. If nothing is
     * found, the arraylist will be empty.
     * 
     * @param ticker        Ticker symbol
     * @param name          Equity name
     * @param index         Equity index
     * @return              ArrayList of holdings
     */
    public ArrayList<Holding> searchHoldings(String ticker, String name, String index)
    {
        CSVController csvC = new CSVController();
        
        ArrayList<Holding> holdings = csvC.getHoldingList();
        
        // To hold results
        ArrayList<Holding> results = new ArrayList<Holding>();
        
        String holdingTicker;
        String holdingName;
        ArrayList<String> holdingIndex;
        
        for(Holding h : holdings)
        {
            holdingTicker = h.getTickerSymbol();
            holdingName = h.getName();
            holdingIndex = h.getPartOf();
            
            if (holdingTicker.contains(ticker) ||
                    holdingName.contains(name) ||
                    holdingIndex.contains(index))
            {
                results.add(h);
            }
        }
        
        return results;
    }

    
    /*
     * return Search a holding from the portfolio
     */
    public Holding searchHolding(Holding holding){
        
        //TODO Search for owned
        if (holding instanceof WatchedStock){
            ArrayList<WatchedStock> watchedStocks = User.getInstance().getPortfolio().getWachededList();
            for(WatchedStock w : watchedStocks){
                if (((WatchedStock)holding).getTickerSymbolReal().equals(w.getTickerSymbolReal())){
                    return w;
                }
            }

        }
                return null;
    }
    
    
    
    
    public Holding searchHolding(String ticker)
    {
        CSVController csvC = new CSVController();
        ArrayList<Holding> holdings = csvC.getHoldingList();

        for (Holding h : holdings)
        {
            if (h.getTickerSymbol().equals(ticker))
            {
                return h;
            }
        }

        return new Stock();

    }

    /**
     * Remove a holding from the portfolio.
     *
     * @param pf        Portfolio
     * @param hold      Holding to remove
     */
    public void removeHolding(Portfolio pf, Holding hold)
    {
        pf.getHoldings().remove(hold);
        notifyObserver();
    }
    
    public void removeWatchedHolding(Portfolio pf, Holding hold)
    {
        pf.getWatchedHoldings().remove(hold);
    }
    
    /**
     * Add some money to a bank account.
     * 
     * @param bank      Bank to which to add money
     * @param amount    Amount of money
     */
    public void bankAddMoney(Bank bank, double amount)
    {
        double currentVal = bank.getInitPrice();
        bank.setInitPrice(currentVal + amount);
    }
    
    /**
     * Remove some money from a bank account. If the amount
     * specified is greater than the amount left, the withdrawal
     * will not occur and the function will return false.
     * 
     * @param bank      Bank from which to remove money
     * @param amount    Amount of money
     * 
     * @return          True if success, false if not
     */
    public boolean bankRemoveMoney(Bank bank, double amount)
    {
        double currentVal = bank.getInitPrice();
        /*double newVal = // don't let account balance drop below 0
                (currentVal - amount) >= 0 ? (currentVal - amount) : 0;*/
        // If withdrawing amount would put the user in debt, do not
        // perform the withdrawal and return false
        if ((currentVal - amount) < 0)
        {
            return false;
        }
        bank.setInitPrice(currentVal - amount);
        return true;
    }
    
    /**
     * Transfer some money between bank accounts. If the amount
     * specified is greater than the amount left in the "from"
     * account, this function will fail and return false.
     * 
     * @param from      Bank to withdraw money from
     * @param to        Bank to deposit money into
     * @param amount    Amount of money
     * 
     * @return          True if success, false if not
     */
    public boolean bankTransferMoney(Bank from, Bank to, double amount)
    {
        double fromCurrentVal = from.getInitPrice();
        /*double transfer = 
                fromCurrentVal < amount ? fromCurrentVal : amount;*/
        // If the "from" bank doesn't have enough money, fail
        if (fromCurrentVal < amount)
        {
            return false;
        }
        bankRemoveMoney(from, amount);
        bankAddMoney(to, amount);
        return true;
    }

    /**
     * Print out a string representation of a portfolio.
     *
     * @param pf        Portfolio
     */
    public void printPortfolio(Portfolio pf)
    {
        String[][] pfArray = prepareHoldingsForTable(pf.getHoldings());

        for (int i = 0; i < pfArray.length; i++)
        {
            for (int j = 0; j < pfArray[i].length; j++)
            {
                System.out.print(pfArray[i][j] + " ");
            }
            System.out.println();
        }
    }
    
    
    /**
     * Return all the owned stocks
     */
    public ArrayList<Holding> getOwnedStocks(Portfolio pf){
        ArrayList<Holding> stocks = new ArrayList<Holding>();
        for (int i = 0; i < pf.getHoldings().size(); i++)
       
            if (pf.getHoldings().get(i).getTickerSymbol() == "!OWNED"){
                stocks.add(pf.getHoldings().get(i));
            }

        
        return stocks;
    }
   
    /**
     * Return all the owned stocks
     */
    public ArrayList<OwnedStock> getOwnedStocks_v2(Portfolio pf){
        ArrayList<OwnedStock> stocks = new ArrayList<OwnedStock>();
        for (int i = 0; i < pf.getHoldings().size(); i++)
       
            if (pf.getHoldings().get(i).getTickerSymbol() == "!OWNED"){
                stocks.add((OwnedStock) pf.getHoldings().get(i));
            }

        
        return stocks;
    }
    /**
     * Return all the watched stocks
     */
    public ArrayList<WatchedStock> getWatchedStocks(Portfolio pf){
        ArrayList<WatchedStock> stocks = pf.getWatchedHoldings();
        return stocks;
    }
    

    
    /**
     * Return all the banks
     */
    public ArrayList<Holding> getBank(Portfolio pf){
        ArrayList<Holding> banks = new ArrayList<Holding>();
        for (int i = 0; i < pf.getHoldings().size(); i++)
       
            if (pf.getHoldings().get(i).getTickerSymbol() == "!BANK"){
                banks.add(pf.getHoldings().get(i));
            }

        
        return banks;
    }

    
    
    @Override
    public void register(Observer o) {
        this.observers.add(o);
        
        
    }

    @Override
    public void unregister(Observer o) {
    	// TODO Auto-generated method stub
        this.observers.remove(observers.indexOf(0));
        
    }

    @Override
    public void notifyObserver() {
        // TODO Auto-generated method stub
    	// Cycle through all observers and notifies them of
        // price changes
        
        for(Observer observer : observers){
            observer.update();   
        }
    }
    
    
    public Boolean updateHolding(Transaction t){
    	User user = User.getInstance();
    	ArrayList<Holding> portfolio = user.getPortfolio().getHoldings();
    	Holding update = null;
    	for (Holding h : portfolio)
        {
    		String tick = h.getTickerSymbol();
    		if (tick.charAt(0) == '!'){
    			if (tick.equals("!OWNED")){
    				OwnedStock os = (OwnedStock)h;
    				if(os.getNameReal().equals(t.getName())){
    	                update = h;
    	                break;
                	}
    			}
            }else{
            	if(h.getName().equals(t.getName())){
	                update = h;
	                break;
            	}
            }
        }
    	if (update == null){
    		Bank from = null;
    		if (t.getTickerSymbol().equals("!BANK")){
    			Bank into = null;
    			for (Holding h : user.getPortfolio().getHoldings()){
    				if (h.getTickerSymbol().equals("!BANK")){
    					if (h.getName().equals(t.getName())){
    						into = (Bank)h;
    					}else if(h.getName().equals(t.getBankName())){
    						from = (Bank)h;
    					}
    				}
    			}
    			Double amount = t.getPrice() * t.getSharesPurchased();
    			if (from != null && into != null){
    				return bankTransferMoney(from, into, amount);
    			}else if (into != null && t.getBankName().equals("!DEPOSIT")){
    				user.getPortfolio().addHolding((Holding)into);
    				return true;
    			}
    		}else{
    			for (Holding h : user.getPortfolio().getHoldings()){
    				if (h.getTickerSymbol().equals("!BANK") && h.getName().equals(t.getBankName())){
						from = (Bank)h;
    				}
    			}
    			Holding search = searchHolding(t.getTickerSymbol());
    			update = new OwnedStock(t.getTickerSymbol(), t.getName(), t.getPrice(), t.getSharesPurchased(), t.getDate(), search.getPartOf());
    			Double amount = t.getSharesPurchased() * t.getPrice();
    			if (amount >= 0){
	    			if (bankRemoveMoney(from, amount)){
	    				user.getPortfolio().addHolding(update);
	    				return true;
	    			}else{
	    				return false;
	    			}
    			}else{
    				return false;
    			}
    		}
    	}else{
    		Bank from = null;
    		if (t.getBankName().equals("!DEPOSIT")){
    			Double amount = t.getPrice();
    			if (amount > 0){
    				bankAddMoney((Bank)update, amount);
    				return true;
    			}else{
    				return bankRemoveMoney((Bank)update, amount);
    			}
    		}
    		for (Holding h : user.getPortfolio().getHoldings()){
				if (h.getTickerSymbol().equals("!BANK") && h.getName().equals(t.getBankName())){
					from = (Bank)h;
				}
			}
    		if (update.getTickerSymbol().equals("!BANK")){
    			Double amount = t.getPrice();
    			if (amount > 0){
    				return bankTransferMoney(from, (Bank)update, amount);
    			}
    		}
    		Double amount = t.getSharesPurchased() * t.getPrice();
    		OwnedStock current = (OwnedStock)update;
    		if (amount > 0){
    			Boolean transfer = bankRemoveMoney(from, amount);
    			if (transfer){
    				current.setSharesOwned(current.getSharesOwned() + t.getSharesPurchased());
    				update = current;
    			}
    			return transfer;
    		}else{
    			Double sold = t.getSharesPurchased() * -1;
    			if (current.getSharesOwned() >= sold){
    				current.setSharesOwned(current.getSharesOwned() - sold);
    				bankAddMoney(from, amount);
    				if (current.getSharesOwned() == 0){
    					user.getPortfolio().removeHolding(current);
    				}
    				return true;
    			}
    			return false;
    		}
    	}
    	return false;
    }
    
    public void recordTransaction(String tickerSymbol, String name, double price, double sharesPurchased, String bankname){
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date dateObject = new Date();
        String date = dateFormat.format(dateObject);
        Transaction tran = new Transaction(tickerSymbol, name, price, sharesPurchased, date, bankname);
        if (updateHolding(tran)){
	        UndoRedo undo = UndoRedo.getInstance();
	        undo.pushUndoArray(tran);
	        User user = User.getInstance();
	        user.addTransaction(tran);
        }else{
        	System.out.println("ERROR: transaction failed");
        }
    }
    
    public void undoTransaction(){
    	UndoRedo ur = UndoRedo.getInstance();
    	if (ur.getUndoable()){
    		Transaction undo = ur.popUndoArray();
    		undo.setSharesPurchased(undo.getSharesPurchased() * -1);
    		if (! updateHolding(undo)){
    			ur.popRedoArray(); //undo the changes to UndoRedo
    			System.out.println("Undo failed");
    		}else{
    			undo.setSharesPurchased(undo.getSharesPurchased() * -1);
    			User.getInstance().removeTransaction(undo);
    			notifyObserver();
    		}
    	}
    }
    
    public void redoTransaction(){
    	UndoRedo ur = UndoRedo.getInstance();
    	if (ur.getRedoable()){
    		Transaction redo = ur.popRedoArray();
    		if (! updateHolding(redo)){
    			ur.popUndoArray(); //undo the changes to UndoRedo
    			System.out.println("Undo failed");
    		}else{
    			User.getInstance().addTransaction(redo);
    			notifyObserver();
    		}
    	}
    }
    
    public Boolean isRedoable(){
    	return UndoRedo.getInstance().getRedoable();
    }
    
    public Boolean idUndoable(){
    	return UndoRedo.getInstance().getUndoable();
    }
    
    
    class AutoUpdate extends TimerTask{
        public void run(){
            updateHoldings(User.getInstance().getPortfolio());

        }
    }
}