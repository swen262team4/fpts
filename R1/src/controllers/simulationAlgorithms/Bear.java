package controllers.simulationAlgorithms;

import controllers.simulationTimeframes.Timeframe;

public class Bear extends Algorithm{
	public Double calculateStepSize(double ammount, double percentAnnum){
		Timeframe frame = Timeframe.getInitializedTimeframe();
		double stepVal = ammount * percentAnnum / 100;
		stepVal = stepVal / frame.getStepsPerYear();
		stepVal = stepVal * -1;
		return stepVal;
	}
}
