package controllers.simulationAlgorithms;

import java.util.ArrayList;
import java.util.HashMap;

import controllers.simulationTimeframes.Day;
import controllers.simulationTimeframes.Month;
import controllers.simulationTimeframes.Timeframe;
import controllers.simulationTimeframes.Week;
import controllers.simulationTimeframes.Year;
import models.Holding;
import models.OwnedStock;
import models.Stock;
import models.WatchedStock;

public abstract class Algorithm {
	public static Algorithm instance;
	
	public static Algorithm getInitializedAlgorithm(){
		if (instance == null){
			instance = (Algorithm)new NoChange();
		}
		return instance;
	}
	
	public static void Instantiate(controllers.SimulationController.algorithmType size){
		switch (size){
		case NO_CHANGE:
			instance = (Algorithm)new NoChange();
			break;
		case BEAR:
			instance = (Algorithm)new Bear();
			break;
		case BULL: 
			instance = (Algorithm)new Bull();
			break;
		}
	}
	
	public abstract Double calculateStepSize(double ammount, double percentAnnum);//Implement in subclass
	
	public HashMap<String, Double> generateStep(ArrayList<String> stocks, HashMap<String, Double> current, HashMap<String, Double> steps){
		HashMap<String, Double> map = new HashMap<String, Double>();
		for (String name : stocks){
			double newVal = Math.round((current.get(name) + steps.get(name))*100.0)/100.0;
			map.put(name, newVal);
		}
		return map;
	}
	
	public ArrayList<HashMap<String, Double> > getSimulation(ArrayList<Holding> stocks, double percentAnnum, int durration){
		ArrayList<String> names = new ArrayList<String>();
		HashMap<String, Double> steps = new HashMap<String, Double>();
		HashMap<String, Double> current = new HashMap<String, Double>();
		for (Holding holding : stocks){
			if (holding.getTickerSymbol().equals("!OWNED")){
				OwnedStock ownedstock = (OwnedStock)holding;
				names.add(ownedstock.getTickerSymbolReal());
				steps.put(ownedstock.getTickerSymbolReal(), calculateStepSize(ownedstock.getInitPriceReal(), percentAnnum));
				current.put(ownedstock.getTickerSymbolReal(), ownedstock.getInitPriceReal());
			}else if (holding.getTickerSymbol().equals("!BANK")){
				continue;
			}else if (holding.getTickerSymbol().equals("!MM")){
				continue;
			}else{
				WatchedStock stock = (WatchedStock)holding;
				names.add(stock.getTickerSymbol());
				steps.put(stock.getTickerSymbol(), calculateStepSize(stock.getInitPrice(), percentAnnum));
				current.put(stock.getTickerSymbol(), stock.getInitPrice());
			}
		}
		ArrayList<HashMap<String, Double> > simulation = new ArrayList<HashMap<String, Double> >();
		for (int i = 0; i <= durration; i++){
			simulation.add(current);
			current = generateStep(names, current, steps);
		}
		return simulation;
	}
}