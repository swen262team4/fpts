package controllers.simulationAlgorithms;

import controllers.simulationTimeframes.Timeframe;

public class Bull extends Algorithm{
	public Double calculateStepSize(double ammount, double percentAnnum){
		Timeframe frame = Timeframe.getInitializedTimeframe();
		double stepVal = ammount * percentAnnum / 100;
		stepVal = stepVal / frame.getStepsPerYear();
		return stepVal;
	}
}
