package models;

import java.util.ArrayList;

/**
 * @author TeamNoPromises
 *
 */
public abstract class Holding {
    
    private String tickerSymbol;
    private String name;
    private double initPrice;
    private ArrayList<String> partOf;

    public Holding(){
        this.tickerSymbol = "";
        this.name = "";
        this.initPrice = 0;
        this.partOf = new ArrayList<String>();
    }
    
    public String getTickerSymbol(){
        return tickerSymbol;
    }
    
    public String getName(){
        return name;
    }
    
    public double getInitPrice(){
        return initPrice;
    }
    
    public void setInitPrice(double newPrice){
        this.initPrice = newPrice;
    }
    public String getInitPriceString(){
    	return Double.toString(initPrice);
    }
    
    public ArrayList<String> getPartOf(){
        return partOf;
    }
}//Holding