package models;

import java.util.ArrayList;

/*
 * Stock Model
 */
public class Stock extends Holding{
	private String tickerSymbol;
	private String name;
    private double initPrice;
    private ArrayList<String> partOf;
    //number of shares
    private int shares = 0;

    public String getTickerSymbol() {
		return tickerSymbol;
	}

	public void setTickerSymbol(String tickerSymbol) {
		this.tickerSymbol = tickerSymbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getInitPrice() {
		return initPrice;
	}
	
	public String getInitPriceString() {
		return Double.toString(initPrice);
	}

	public void setInitPrice(double initPrice) {
		this.initPrice = initPrice;
	}

	public ArrayList<String> getPartOf() {
		return partOf;
	}

	public void setPartOf(ArrayList<String> partOf) {
		this.partOf = partOf;
	}

	public Stock(String tickerSymbol, String name, double initPrice, ArrayList<String> partOf){
        this.tickerSymbol = tickerSymbol;
        this.name = name;
        this.initPrice = initPrice;
    	this.partOf = partOf;
    }
	
	public Stock(String tickerSymbol){
	    this.tickerSymbol = tickerSymbol;
	}
	
	public Stock(){}

	
    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }
}