package models;

import java.util.ArrayList;
import java.util.regex.*;

public class OwnedStock extends Holding{
	private String tickerSymbol;
	private String name;
    private double initPrice;
    private double sharesOwned;
    private String date;
    private ArrayList<String> partOf;

    //This set of getters are the ones that r=are used by holding
    public String getTickerSymbol(){
    	return "!OWNED";
    }
	
    public String getName(){
	    return tickerSymbol;
    }
    
	public String getInitPriceString(){
		return name;
	}
	
	public double getInitPrice(){
		return initPrice;
	}
	
	public ArrayList<String> getPartOf(){
		ArrayList<String> temp = new ArrayList<String>();
		temp.add(Double.toString(initPrice));
		temp.add(Double.toString(sharesOwned));
		temp.add(date);
		temp.addAll(partOf);
		return temp;
	}
	//end extended methods.
	//all methods that follow are for this specific implementation
    
    public String getTickerSymbolReal() {
		return tickerSymbol;
	}

	public void setTickerSymbol(String tickerSymbol) {
		this.tickerSymbol = tickerSymbol;
	}

	public String getNameReal() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getInitPriceReal() {
		return initPrice;
	}

	public void setInitPrice(double initPrice) {
		this.initPrice = initPrice;
	}
	
	public ArrayList<String> getPartOfReal() {
		return partOf;
	}

	public void setPartOf(ArrayList<String> partOf) {
		this.partOf = partOf;
	}

	public double getSharesOwned() {
		return sharesOwned;
	}

	public void setSharesOwned(double sharesOwned) {
		this.sharesOwned = sharesOwned;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public OwnedStock(String tickerSymbol, String name, double initPrice, double sharesOwned, String date, ArrayList<String> partOf){
        this.tickerSymbol = tickerSymbol;
        this.name = name;
        this.initPrice = initPrice;
        this.partOf = partOf;
        this.date = date;
        this.sharesOwned = sharesOwned;
    }
}