package models;


import java.util.ArrayList;

public class User
{
    // This will be a singleton class
    private static User instance = null;
    private String ID;
    private String password;
    private String userCSV;
    private Portfolio pf;
    private ArrayList<Transaction> transactionList;
    private static boolean newUser = true;

    /**
     * Singleton constructor
     */
    private User() {}

    /**
     * Gets the current instance of the user singleton.
     *
     * @return      User singleton
     */
    public static User getInstance()
    {

        if (instance == null)
        {

            instance = new User();
            instance.transactionList = new ArrayList<Transaction>();
            instance.pf = new Portfolio(instance);
        }
        return instance;
    }

    /**
     * Set the user's ID (username)
     *
     * @param id    Username string
     */
    public void setID(String id)
    {
        this.ID = id;
    }

    /**
     * Returns the user singleton's ID (username)
     *
     * @return      Username string
     */
    public String getID()
    {
        return this.ID;
    }

    /**
     * Set the user's password
     *
     * @param pw    Password string
     */
    public void setPassword(String pw)
    {
        this.password = pw;
    }

    /**
     * Return the user's password
     *
     * @return      Password string
     */
    public String getPassword()
    {
        return this.password;
    }

	public void addTransaction(Transaction transaction) {
		this.transactionList.add(transaction);
	}

	public void addTransaction(ArrayList<Transaction> transaction) {
		this.transactionList.addAll(transaction);
	}

    /**
     * Associate a portfolio object with the user
     *
     * @param pf    Portfolio object
     */
    public void setPortfolio(Portfolio pf)
    {
        this.pf = pf;
    }

    /**
     * Return the user's portfolio
     *
     * @return      Portfolio object
     */
    public Portfolio getPortfolio()
    {
        return this.pf;
    }

    /**
     * Associate a transaction list with this user
     *
     * @param tList Transaction list
     */
    public void setTransactionList(ArrayList<Transaction> tList)
    {
        this.transactionList = tList;
    }

    /**
     * Get this user's transaction list
     *
     * @return      Transaction list
     */
    public ArrayList<Transaction> getTransactionList()
    {
        return this.transactionList;
    }

    public static boolean isNewUser() {
        return newUser;
    }

    public static void setNewUser(boolean newUser) {
        User.newUser = newUser;
    }

    public String getUserCSV() {
        return userCSV;
    }

    public void setUserCSV(String userCSV) {
        this.userCSV = userCSV;
    }
    
    public void removeTransaction(Transaction t){
		for (Transaction tran : this.transactionList){
			if (t.checkEqual(tran)){
				this.transactionList.remove(tran);
				break;
			}
		}
	}
}
