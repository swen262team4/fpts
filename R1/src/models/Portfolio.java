package models;

import java.util.ArrayList;


/*
 * Portfolio Model
 * This contains user holdings
 */
public class Portfolio {
    
	private User owner;
	private ArrayList<Holding> holdings;
	private ArrayList<Transaction> transactions;
	private ArrayList<WatchedStock> watchlist;
	

	public Portfolio(User owner){
		this.owner = owner;
		this.holdings = new ArrayList<Holding>();
		this.watchlist = new ArrayList<WatchedStock>();
		this.transactions = new ArrayList<Transaction>();


	}

	public void addHolding(Holding holding){
		this.holdings.add(holding);
	}

	public void addHolding(ArrayList<Holding> holding){
		this.holdings.addAll(holding);
	}

	public void importNewList(ArrayList<Holding> holdings){
		this.holdings = holdings;
	}

	public void importAddList(ArrayList<Holding> holdings){
		this.holdings.addAll(holdings);
	}
	

	
	public void addWatchlistHolding(ArrayList<WatchedStock> stocks)
	{
	    this.watchlist.addAll(stocks);
	}
	
    public void deleteWatchedHolding(WatchedStock stock)
    {
        this.watchlist.remove(stock);
    }
	
	public void importNewWatchlist(ArrayList<WatchedStock> stocks){
        this.watchlist = stocks;
    }

    public void importAddWatchlist(ArrayList<WatchedStock> stocks){
        this.watchlist.addAll(stocks);
    }

	public ArrayList<Holding> getHoldings(){
		return this.holdings;
	}
	
	public ArrayList<WatchedStock> getWatchedHoldings()
	{
	    return this.watchlist;
	}
	
	public ArrayList<Transaction> getTransactions()
	{
	    return this.transactions;
	}
	
	public void addTransaction(Transaction t)
	{
	    this.transactions.add(t);
	}

	public User getOwner(){
		return this.owner;
	}

	public void removeHolding(Holding h) {
		this.holdings.remove(h);
	}

    public void addWatchlistHolding(WatchedStock stock)
    {
        this.watchlist.add(stock);
    }
    
    public ArrayList<WatchedStock> getWachededList() {
        return watchlist;
    }

    public void setWachededList(ArrayList<WatchedStock> wachededList) {
        this.watchlist = wachededList;
    }
}
