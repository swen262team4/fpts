package models;

import java.util.ArrayList;

/*
 * Undo redo subsystem
 */
public class UndoRedo {
	private static UndoRedo instance = null;
	private ArrayList<Transaction> undoArray;
	private ArrayList<Transaction> redoArray;
	
	public static UndoRedo getInstance()
    {

        if (instance == null)
        {
            instance = new UndoRedo();
        }
        return instance;
    }
	
	public UndoRedo(){
		undoArray = new ArrayList<Transaction>();
		redoArray = new ArrayList<Transaction>();
	}

	public ArrayList<Transaction> getUndoArray() {
		return undoArray;
	}
	
	public Transaction popUndoArray(){
		Transaction undo = undoArray.get(undoArray.size()-1);
		undoArray.remove(undoArray.size()-1);
		redoArray.add(undo);
		return undo;
	}
	
	public void pushUndoArray(Transaction t){
		undoArray.add(t);
		redoArray = new ArrayList<Transaction>();
	}

	public Boolean getRedoable() {
		return !redoArray.isEmpty();
	}
	
	public Boolean getUndoable() {
		return !undoArray.isEmpty();
	}
	
	public Transaction popRedoArray(){
		Transaction redo = new Transaction();
		if (!redoArray.isEmpty()){
			redo = redoArray.get(redoArray.size()-1);
			redoArray.remove(redoArray.size()-1);
			undoArray.add(redo);
		}
		return redo;
	}
}