package models;

/**
 * @author Max
 *
 */
public class Transaction{
    private String tranMark;
	private String tickerSymbol;
	private String name;
	private Double price;
	private Double sharesPurchased;
	private String date;
	private String bankName;
	
    public Transaction(String ticker, String name, Double price, Double shares, String date, String bank){
        this.setTranMark("!T");
        this.tickerSymbol = ticker;
        this.name = name;
        this.price = price;
        this.sharesPurchased = shares;
        this.date = date;
        this.bankName = bank;
    }
    
    public Transaction(){}

	public String getTickerSymbol() {
		
		return tickerSymbol;
	}

	public void setTickerSymbol(String tickerSymbol) {
		this.tickerSymbol = tickerSymbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getSharesPurchased() {
		return sharesPurchased;
	}

	public void setSharesPurchased(Double sharesPurchased) {
		this.sharesPurchased = sharesPurchased;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

    public String getTranMark() {
        return tranMark;
    }

    public void setTranMark(String tranMark) {
        this.tranMark = tranMark;
    }

	public boolean checkEqual(Transaction t) {
		return this.tickerSymbol.equals(t.tickerSymbol)
				&& this.name.equals(t.name)
				&& this.price == t.price
				&& this.sharesPurchased == t.sharesPurchased
				&& this.date.equals(t.date)
				&& this.bankName.equals(t.bankName);
	}   
}