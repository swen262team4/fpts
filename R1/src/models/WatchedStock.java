package models;

import java.util.ArrayList;

public class WatchedStock extends Holding{
    private String tickerSymbol;
    private String name;
    private double initPrice;
    private String date;
    private ArrayList<String> partOf;
   
    private double highTriggerPoint;
    private double lowTriggerPoint;
    public enum Status { NORMAL, HIGH, LOW};
    private Status status;

    
    /*
     * Constructor for a watched stock
     */
    public WatchedStock(String tickerSymbol, String name, double initPrice, 
            ArrayList<String> partOf, double highTriggerPoint, double lowTriggerPoint){
        this.tickerSymbol = tickerSymbol;
        this.name = name;
        this.initPrice = initPrice;
        this.partOf = partOf;
        this.setHighTriggerPoint(highTriggerPoint);
        this.setLowTriggerPoint(lowTriggerPoint);
        this.setStatus("NORMAL");      
    }

    public WatchedStock(String tickerSymbol, String name, double initPrice, 
            ArrayList<String> partOf, double highTriggerPoint, double lowTriggerPoint, String status){
        this.tickerSymbol = tickerSymbol;
        this.name = name;
        this.initPrice = initPrice;
        this.partOf = partOf;
        this.setHighTriggerPoint(highTriggerPoint);
        this.setLowTriggerPoint(lowTriggerPoint);
        this.setStatus(status);      
    }
    //This set of getters are the ones that r=are used by holding
    public String getTickerSymbol(){
        return "!WATCH";
    }
    
    public String getName(){
        return tickerSymbol;
    }
    
    public String getInitPriceString(){
        return name;
    }
    
    public double getInitPrice(){
        return initPrice;
    }
    
    public ArrayList<String> getPartOf(){
        ArrayList<String> temp = new ArrayList<String>();
        temp.add(Double.toString(initPrice));
        temp.add(date);
        temp.addAll(partOf);
        return temp;
    }
    //end extended methods.
    //all methods that follow are for this specific implementation
    
    public String getTickerSymbolReal() {
        return tickerSymbol;
    }

    public void setTickerSymbol(String tickerSymbol) {
        this.tickerSymbol = tickerSymbol;
    }

    public String getNameReal() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getInitPriceReal() {
        return initPrice;
    }

    public void setInitPrice(double initPrice) {
        this.initPrice = initPrice;
        
        if (this.initPrice < this.lowTriggerPoint){
            this.status = Status.LOW;
        }else if(this.initPrice > this.highTriggerPoint){
            this.status = Status.HIGH;
        }else{
            this.status = Status.NORMAL;
        }
        
        
    }
    
    public ArrayList<String> getPartOfReal() {
        return partOf;
    }

    public void setPartOf(ArrayList<String> partOf) {
        this.partOf = partOf;
    }

    public double getLowTriggerPoint() {
        return lowTriggerPoint;
    }

    public void setLowTriggerPoint(double lowTriggerPoint) {
        this.lowTriggerPoint = lowTriggerPoint;

    }

    public double getHighTriggerPoint() {
        return highTriggerPoint;
    }

    public void setHighTriggerPoint(double lowTriggerPoint) {
        this.highTriggerPoint = lowTriggerPoint;

    }
    public Status getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = Status.valueOf(status);
    }


}