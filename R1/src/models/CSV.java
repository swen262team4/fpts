package models;

import java.util.ArrayList;


/*
 * CSV Model
 */
public class CSV {
	private ArrayList<Holding> holdings;
	private ArrayList<Transaction> transactions;
	private ArrayList<WatchedStock> watch;

	public CSV(){
		this.holdings = new ArrayList<Holding>();
		this.transactions = new ArrayList<Transaction>();
		this.watch = new ArrayList<WatchedStock>();
	}

	public ArrayList<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(ArrayList<Transaction> transactions) {
		this.transactions = transactions;
	}

	public void addTransaction(Transaction tran){
		this.transactions.add(tran);
	}

	public void addHolding(Holding holding){
		this.holdings.add(holding);
	}

	public void setHolding(ArrayList<Holding> holdings){
		this.holdings = holdings;
	}

	public ArrayList<Holding> getHoldingList(){
		return holdings;
	}

	public ArrayList<WatchedStock> getWatch() {
		return watch;
	}

	public void setWatch(ArrayList<WatchedStock> watch) {
		this.watch = watch;
	}
	
	public void addWatch(WatchedStock stock){
		this.watch.add(stock);
	}

	public String toString(){
		StringBuilder builder = new StringBuilder();
		for (Holding holding : holdings){
		    builder.append(holding.getTickerSymbol() + ", " + holding.getName() + ", " + holding.getInitPriceString());
			for (String part : holding.getPartOf()){
				builder.append(", " + part);
			}
			builder.append("\n");

		}
		return builder.toString();
	}
}
