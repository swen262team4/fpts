package models;

import java.util.ArrayList;

public class MoneyMarket extends Holding {
	private String tickerSymbol; //this should always be !BANK
    private String name;  //Bank Name
    private double initPrice;  //Money in the bank account
    private int routingNumber; 
    private int accountNumber;
    private String date;
    private ArrayList<String> partOf;

    public MoneyMarket(String name, double balance, int routing, int number, String date){
        this.tickerSymbol = "!MM";
    	this.name = name;
        this.initPrice = balance;
        this.routingNumber = routing;
        this.accountNumber = number;
        this.date = date;
        ArrayList<String> part = new ArrayList<String>();
        part.add(Integer.toString(routing));
        part.add(Integer.toString(number));
        part.add(date);
        this.partOf = part;
    }
    
    public MoneyMarket(String tickerSymbol, String name, double initPrice, ArrayList<String> partOf){
    	this.tickerSymbol = tickerSymbol;
    	this.name = name;
    	this.initPrice = initPrice;
    	this.partOf = partOf;
    	if (partOf.size() >= 0)
    		this.routingNumber = Integer.parseInt(partOf.get(0));
    	if (partOf.size() >= 1)
    		this.accountNumber = Integer.parseInt(partOf.get(1));
    	if (partOf.size() >= 2)
    		this.date = partOf.get(2);
    }
    
    public MoneyMarket(){
    	this.tickerSymbol = "!MM";
    }

    public String getTickerSymbol(){
    	return tickerSymbol;
    }
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getInitPrice() {
		return initPrice;
	}
	
	public String getInitPriceString() {
		return Double.toString(initPrice);
	}

	public void setInitPrice(double initPrice) {
		this.initPrice = initPrice;
	}

	public int getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(int routingNumber) {
		this.routingNumber = routingNumber;
		this.partOf.set(0, Integer.toString(routingNumber));
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
		this.partOf.set(1, Integer.toString(accountNumber));
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
		this.partOf.set(2, date);
	}
	
	public ArrayList<String> getPartOf(){
		return partOf;
	}
	
	public void setPartOf(ArrayList<String> partOf){
		this.partOf = partOf;
		if (partOf.size() >= 0)
    		this.routingNumber = Integer.parseInt(partOf.get(0));
    	if (partOf.size() >= 1)
    		this.accountNumber = Integer.parseInt(partOf.get(1));
    	if (partOf.size() >= 2)
    		this.date = partOf.get(2);
	}
}