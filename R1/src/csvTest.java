import java.util.ArrayList;

import controllers.CSVController;
import controllers.PortfolioController;
import models.Bank;
import models.CSV;
import models.Holding;
import models.MoneyMarket;
import models.OwnedStock;
import models.Portfolio;
import models.Stock;
import models.Transaction;
import models.User;

public class csvTest {
	public static void main(String[] args) {
        Bank bank1 = new Bank("Bank of America", 87.63, 123456789, 987654321, "20151010");
        Bank bank2 = new Bank("Bank of JOHN CENA", 1000000000.00, 133333337, 133333337, "20020220");

        MoneyMarket mm1 = new MoneyMarket("MONEY MONEY MONEY, Inc.", 12.34, 1234, 1234, "20000102");
        MoneyMarket mm2 = new MoneyMarket("Mo` Money Mo` Problems", 43.21, 4321, 4321, "20000102");


        ArrayList<String> partOf1 = new ArrayList<String>();
        partOf1.add("DOW");
        OwnedStock stock1 = new OwnedStock("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", partOf1);

        ArrayList<String> partOf2 = new ArrayList<String>();
        partOf2.add("NasDaq");
        partOf2.add("OTHER");
        OwnedStock stock2 = new OwnedStock("LIST", "Lists for Listing Lists of Lists & Co.", 34.56, 55.55, "20151015", partOf2);


        ArrayList<String> partOf3 = new ArrayList<String>();
        partOf3.add("dow");
        Stock stock3 = new Stock("HELP", "Churches of America and Southern Canada", 34.56, partOf3);



        ArrayList<String> partOf4 = new ArrayList<String>();
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        Stock stock4 = new Stock("TTT", "Trouble in Terrorist Town, a Mod for Source.", 34.56, partOf4);

        Transaction t1 = new Transaction("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", "Bank of America");
        Transaction t2 = new Transaction("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", "Bank of JOHN CENA");


        CSV csv = new CSV();
        csv.addHolding(bank1);
        csv.addHolding(bank2);
        csv.addHolding(mm1);
        csv.addHolding(mm2);
        csv.addHolding(stock1);
        csv.addHolding(stock2);
        csv.addHolding(stock3);
        csv.addHolding(stock4);
        csv.addTransaction(t1);
        csv.addTransaction(t2);

        CSVController c = new CSVController();
        c.createCSV(csv, "test_csv.csv");

        System.out.println("Exported csv file to test_csv.txt\n" + csv.toString() + "\nExtracting equities.csv");

        csv = c.openCSV("equities.csv");

        System.out.println("Printing equities.csv\n" + csv.toString());
        System.out.println("done testing");
        
        PortfolioController pf = new PortfolioController();
        
        User user = User.getInstance();
        user.setID("trs3548");
        Portfolio p = new Portfolio(user);
        
        c.addCSVToPortfolio();
        
        pf.printPortfolio(user.getPortfolio());
        
        c.getPortfolio();
        
        c.createCSV("users\\" + user.getID() + ".csv");
        
        //Holding testHolding = new Holding();
        
	}       
}