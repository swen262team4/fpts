package views;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JMenuBar;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPopupMenu;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import models.Bank;
import observer.Observer;

public class BankPanel extends MainPanel{
    
   
    private JTextField txtName;
    private JTextField txtAccount;
    private JTextField txtRounting;
    private JTextField txtBalance;

    /**
     * Create the panel
     * This panel contails all the banks from user portfolio
     */
    public BankPanel() {
        init();
    }

    public void init(){
        setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        panel.add(tabbedPane, BorderLayout.CENTER);
        
        JPanel bankTabbedPanel = new BankTabbedPanel();
        tabbedPane.addTab("Bank", null, bankTabbedPanel, null);
        
        JPanel panel_2 = new JPanel();
        tabbedPane.addTab("New Bank", null, panel_2, null);
        panel_2.setLayout(null);
        
        
        
        
        JLabel lblBankName = new JLabel("Bank Name");
        lblBankName.setBounds(24, 45, 93, 16);
        panel_2.add(lblBankName);
        
        txtName = new JTextField();
        txtName.setText("name");
        txtName.setBounds(129, 40, 130, 26);
        panel_2.add(txtName);
        txtName.setColumns(10);
        
        JLabel lblRounting = new JLabel("Rounting #");
        lblRounting.setBounds(24, 93, 80, 16);
        panel_2.add(lblRounting);
        
        JLabel lblBalance = new JLabel("Balance:");
        lblBalance.setBounds(24, 115, 61, 16);
        panel_2.add(lblBalance);
        
        JLabel lblAccount = new JLabel("Account #");
        lblAccount.setBounds(24, 69, 80, 16);
        panel_2.add(lblAccount);
        
        txtAccount = new JTextField();
        txtAccount.setBounds(129, 64, 130, 26);
        panel_2.add(txtAccount);
        txtAccount.setColumns(10);
        
        txtRounting = new JTextField();
        txtRounting.setBounds(129, 88, 130, 26);
        panel_2.add(txtRounting);
        txtRounting.setColumns(10);
        
        txtBalance = new JTextField();
        txtBalance.setBounds(129, 115, 130, 26);
        panel_2.add(txtBalance);
        txtBalance.setColumns(10);
        
        
        /*
         * ADD NEW BANK
         */
        JButton btnAdd = new JButton("Add");
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    String name = txtName.getText();
                    int accountNumber = Integer.parseInt(txtAccount.getText());
                    int routingNumber = Integer.parseInt(txtRounting.getText());
                    double balance = Double.parseDouble(txtBalance.getText());
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    //get current date time with Date()
                    Date date = new Date();

                    String today = dateFormat.format(date);
                    
                    Bank newBank = new Bank(name, balance, routingNumber, accountNumber, today);
                    
                    pc.addHolding(user.getPortfolio(), newBank);
                    JOptionPane.showMessageDialog(null, "Bank Added");
                    txtAccount.setText("");
                    txtRounting.setText("");
                    txtBalance.setText("");
                    
                }catch (Exception error){
                    error.printStackTrace();
                }
            }
        });
        btnAdd.setBounds(59, 152, 117, 29);
        panel_2.add(btnAdd);
        
    }//Init
    
 
    
}
