package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

//import javafx.scene.input.DataFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JMenuBar;

import models.Bank;
import models.Holding;
import models.Stock;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import java.util.ArrayList;


/*
 * This frame is used to buy stock from the stock market
 */
public class StockTransactionFrame extends JFrame {

    private JPanel contentPane;
    private JTextField tfShares;
    private Holding stock;
    private MainPanel panel;
    //private double bankBalance;



    /**
     * Create the frame.
     */
    public StockTransactionFrame(Holding stock) {
        this.stock = stock;


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        panel = new MainPanel();
        contentPane.add(panel, BorderLayout.CENTER);
        panel.setLayout(null);
        init();

    }

    /*
     * Close the frame
     */
    public void close(){
        this.setVisible(false);
        this.dispose();
    }


    public void init(){
        final NumberFormat nf = NumberFormat.getCurrencyInstance();
        JLabel stockName = new JLabel(stock.getName());
        stockName.setBounds(17, 30, 127, 16);
        panel.add(stockName);

        JLabel stockPrice = new JLabel(nf.format(stock.getInitPrice()));
        stockPrice.setBounds(231, 30, 61, 16);
        panel.add(stockPrice);

        JLabel lblTickerSymbol = new JLabel(stock.getTickerSymbol());
        lblTickerSymbol.setBounds(156, 30, 61, 16);
        panel.add(lblTickerSymbol);

        JLabel shares = new JLabel("Shares :");
        shares.setBounds(17, 61, 61, 16);
        panel.add(shares);

        //INPUT for number of shares to buy
        tfShares = new JTextField();
        tfShares.setBounds(84, 56, 94, 26);
        panel.add(tfShares);
        tfShares.setColumns(10);




        JButton btbCancel = new JButton("Cancel");
        btbCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                close();
            }
        });
        btbCancel.setBounds(205, 198, 117, 29);
        panel.add(btbCancel);

        JMenuBar menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, 434, 22);
        panel.add(menuBar);
        
        JLabel label = new JLabel("->");
        menuBar.add(label);

        JMenu mnBank = new JMenu("Bank");
        menuBar.add(mnBank);
        
        JLabel lblBalance_1 = new JLabel("Balance:");
        menuBar.add(lblBalance_1);


        final JLabel lblBankBalance = new JLabel("$0.00");
        menuBar.add(lblBankBalance);

        //Get current bank accounts
        final ArrayList<Holding> banks = panel.pc.getBank(panel.user.getPortfolio());

        ArrayList<JMenuItem> bankMenuItems = new ArrayList<JMenuItem>();


        for(int i = 0; i<banks.size(); i++){
            bankMenuItems.add(new JMenuItem(banks.get(i).getName()));
        }

        final JLabel bankIndex = new JLabel("");

        for(int i = 0; i <bankMenuItems.size(); i++){
            final double balance2 = ((Bank)banks.get(i)).getInitPrice();
            final String index = Integer.toString(i);

            bankMenuItems.get(i).addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent e) {
                    // TODO Auto-generated method stub
                    lblBankBalance.setText("$"+balance2);
                    bankIndex.setText(index);

                }

            });
            mnBank.add(bankMenuItems.get(i));
        }


        ////// THIS SHOWS TRANSACTION ESTIMATION
        JLabel lblShare = new JLabel("Share");
        lblShare.setBounds(83, 105, 61, 16);
        panel.add(lblShare);

        final JLabel shareNumber = new JLabel("");
        shareNumber.setBounds(156, 105, 61, 16);
        panel.add(shareNumber);

        JLabel lblTotal = new JLabel("Total ");
        lblTotal.setBounds(83, 126, 61, 16);
        panel.add(lblTotal);

        final JLabel total = new JLabel("");
        total.setBounds(156, 126, 136, 16);
        panel.add(total);

        JLabel lblBalance = new JLabel("Balance");
        lblBalance.setBounds(83, 151, 61, 16);
        panel.add(lblBalance);

        final JLabel balance = new JLabel("");
        balance.setBounds(156, 151, 136, 16);
        panel.add(balance);


        ////////////////////////////////////////

        //CALCULATING THE CURRENT TRANSACTION
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    int shares = Integer.parseInt(tfShares.getText());
                    shareNumber.setText(tfShares.getText());
                    double totalPrice = shares*stock.getInitPrice();
                    total.setText(nf.format(totalPrice));
                    double bankBalance = Double.parseDouble( lblBankBalance.getText().substring(1,lblBankBalance.getText().length()));
                    double afterBalance =  bankBalance - totalPrice;
                    balance.setText(nf.format(afterBalance));

                    if(afterBalance < 0){
                        JOptionPane.showMessageDialog(null, "Insufficient Fund");
                    }


                }catch (Exception error){
                    JOptionPane.showMessageDialog(null, "Invalid Input");
                    error.printStackTrace();
                }

            }
        });
        btnCalculate.setBounds(190, 56, 117, 29);
        panel.add(btnCalculate);


        //PURCHASING
        JButton btbBuy = new JButton("Purchase");
        btbBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    int shares = Integer.parseInt(tfShares.getText());
                    shareNumber.setText(tfShares.getText());
                    double totalPrice = shares*stock.getInitPrice();
                    total.setText(nf.format(totalPrice));
                    double bankBalance = Double.parseDouble( lblBankBalance.getText().substring(1,lblBankBalance.getText().length()));
                    double afterBalance =  bankBalance - totalPrice;
                    balance.setText(nf.format(afterBalance));

                    if(afterBalance < 0){
                        JOptionPane.showMessageDialog(null, "Insufficient Fund");
                    }else{

                        int index = Integer.parseInt(bankIndex.getText());
                        Bank bank = (Bank)banks.get(index);
                        //PASS INFO TO PORTFOLIO CONTROLLER
                        panel.pc.addTransaction(bank, (Stock)stock, shares);

                        //Notify User
                        JOptionPane.showMessageDialog(null, "Transaction completed");
                        //Close the frame
                        close();
                    }


                }catch (Exception error){
                    JOptionPane.showMessageDialog(null, "Invalid Input");
                    error.printStackTrace();
                }
            }
        });
        btbBuy.setBounds(76, 198, 117, 29);
        panel.add(btbBuy);



    }



}
