package views;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controllers.AccountController;

/*
 * This class will start the program, and provide a login screen
 */
public class FPTS_Run {

    JFrame frame;

    /**
     * Launch the application.
     */
    public static void main(final String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    if (args.length == 2)
                    {
                        if (args[0].equals("-delete"))
                        {
                            AccountController.deleteUser(args[1]);
                        }
                    }
                    FPTS_Run window = new FPTS_Run();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public FPTS_Run() {
        frame = new JFrame();
        frame.setBounds(100, 100, 450, 368);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        /*
         * Account controller to handle user
         */
        final AccountController ac = new AccountController();



        
        //app main frame
        

        
        final JPanel loginPanel = new JPanel();
        loginPanel.setLayout(null);
        loginPanel.setBounds(0, 0, 450, 278);
        frame.getContentPane().add(loginPanel);
        loginPanel.setVisible(true);

        JLabel label = new JLabel("FPTS");
        label.setFont(new Font("Lucida Grande", Font.PLAIN, 44));
        label.setBounds(162, 6, 130, 53);
        loginPanel.add(label);

        JLabel label_1 = new JLabel("Username:");
        label_1.setBounds(122, 80, 73, 16);
        loginPanel.add(label_1);

        final JTextField tfUsername = new JTextField();
        tfUsername.setText("enter your id");
        tfUsername.setColumns(10);
        tfUsername.setBounds(199, 75, 130, 26);
        loginPanel.add(tfUsername);

        JLabel label_2 = new JLabel("Password");
        label_2.setBounds(122, 122, 73, 16);
        loginPanel.add(label_2);

        final JPasswordField passwordField = new JPasswordField();
        passwordField.setBounds(199, 117, 130, 26);
        loginPanel.add(passwordField);

        JButton btnLogin = new JButton("Sign In");
        btnLogin.setBounds(144, 150, 87, 29);
        loginPanel.add(btnLogin);

        JButton button_1 = new JButton("Cancel");
        button_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        button_1.setBounds(231, 150, 87, 29);
        loginPanel.add(button_1);

        JButton btnRegister = new JButton("Register");
        btnRegister.setBounds(175, 179, 117, 29);
        loginPanel.add(btnRegister);
        
        /*
         * This shows any message received from the controller
         */
        final JTextArea taLoginMessage = new JTextArea();
        taLoginMessage.setBounds(93, 226, 294, 16);
        loginPanel.add(taLoginMessage);

       
        
        

        //LOG IN BUTTON PRESSED
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                        String username = tfUsername.getText();
                        
                        String password = new String(passwordField.getPassword());
                                              
                        if(ac.logIn(username,  password) != null){

                            
                            try {
                                //dispose login screen
                                loginPanel.setVisible(false);
                                frame.setVisible(false);                     
                                frame.dispose();
                                //open main screen
                                //pass the authenticated user to the main view for later use
                                FPTSTabbedView window = new FPTSTabbedView();
                                window.getFrame().setVisible(true);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            
                            
                        }else{
                            //Show message received from the controller
                            taLoginMessage.setText(ac.getMessage());
                        }
                        
          

                }catch (Exception error){
                    System.out.println(error);
                }
                
            }
        });
        
        //REGISTER BUTTON PRESSED
        btnRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String username = tfUsername.getText();
                String password = new String(passwordField.getPassword());

                ac.createUser(username,  password);
                
                taLoginMessage.setText(ac.getMessage());
                     
            }
        });

        
    }

}
