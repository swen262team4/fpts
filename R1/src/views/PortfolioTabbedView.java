package views;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import java.awt.BorderLayout;

public class PortfolioTabbedView extends JPanel {

  
    /**
     * Create the panel.
     */
    public PortfolioTabbedView() {
        
        
        setLayout(new BorderLayout(0, 0));
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        add(tabbedPane, BorderLayout.CENTER);
        
        JPanel stock = new StockPanel();
        tabbedPane.addTab("My Stock", null, stock, null);
        
    
        JPanel bank = new BankPanel();
        tabbedPane.addTab("Bank", null, bank, null);
        
        JPanel watchedList = new WatchedStockPanel();
        tabbedPane.addTab("Watched List", null, watchedList, null);
        
        JPanel transation = new TransactionPanel();
        tabbedPane.addTab("Transation", null, transation, null);
        
        JPanel stockMarket = new StockMarketPanel();
        tabbedPane.addTab("Stock Market", null, stockMarket, null);
        

    }
    
}


