package views;

import java.awt.Color;
import java.awt.Component;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import observer.Observer;
import models.Holding;
import models.Stock;
import models.User;
import models.WatchedStock;
import controllers.AccountController;
import controllers.CSVController;
import controllers.PortfolioController;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;



/*
 * Panel to show all the stocks in watch list
 */
public class WatchedStockPanel extends MainPanel implements Observer{
    

    JPanel centerPanel;
    

    /**
     * Create the panel.
     */
    public WatchedStockPanel() {
        setLayout(new BorderLayout(0, 0));
        
        centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);
        centerPanel.setLayout(new BorderLayout(0, 0));
        //register to observe portfolio
        pc.register(this);
        init();
    }
    
    public void init(){
        
        //userCSVFile from mainPanel
        cc.openCSV(userCSVFile);

        JTable table = createTable();
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();
       
    }
    
    
    public void redraw(){
        JTable table = createTable();
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();

    }
    
    
    
    public JTable createTable(){
        
        final ArrayList<WatchedStock> holdings = pc.getWatchedStocks(user.getPortfolio());
  
        int len = 0; //this is used to determine the length of each array.
        for (WatchedStock holding : holdings){
            int size = holding.getPartOfReal().size();
            if (size > len){
                len = holding.getPartOf().size(); //update longest array size
            }
        }
        //now 4 because new button
        len += 5; //this is done because button, status, symbol, name, and initValue are added to the partOf list
        
        int wid = holdings.size();
        
       // System.out.println(wid);
        Object[][] stockTable;
        if (wid== 0){
            stockTable = new String[1][6];
            stockTable[0][0] = "No Watched Stock";
            stockTable[0][1] = "";
            stockTable[0][2] = "";
            stockTable[0][3] = "";
            stockTable[0][4] = "";
            stockTable[0][5] = "";
            
        }else{
            stockTable = new Object[wid][len];
            WatchedStock holding;
            //System.out.println(wid +" "+ len);
            for(int i = 0 ; i < wid; i ++){
                holding = holdings.get(i);
                stockTable[i][0] = "Edit";
                stockTable[i][1] = holding.getStatus();
                stockTable[i][2] = holding.getTickerSymbolReal();
                stockTable[i][3] = holding.getNameReal();
                stockTable[i][4] = holding.getInitPriceReal();
                int j;
                for (j = 0; j < holding.getPartOfReal().size(); j++){
                    stockTable[i][j+5] = holding.getPartOfReal().get(j);
                }
            }
        }
        int columns = stockTable[0].length;
        

        String[] columnNames = new String[columns];
        columnNames[0] = "";
        columnNames[1] = "Status";
        columnNames[2] = "Ticker Symbol";
        columnNames[3] = "Name";
        columnNames[4] = "Value";
        columnNames[5] = "Additional Information";
        for (int i = 6; i < columns; i++){
            columnNames[i] = "";
        }
        
        DefaultTableModel model = new DefaultTableModel(stockTable, columnNames);
        
      
        
        JTable table = new JTable(model);
        
        //get value of the stock at ith row
        
        /*
         * Define action when Buy button is clicked
         */
        Action editTriggerPoints = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                //THIS ALSO THE HOLDING INDEX IN HOLDINGS parsed from the equities.csv
                int modelRow = Integer.valueOf(e.getActionCommand());
                WatchedStock watchedStock = holdings.get(modelRow);

                //TriggerPointDialog will handle the editing 
                new TriggerPointDialog(watchedStock);
            }
            
        };
        

        
        //Assign action to button
        ButtonColumn buttonColumnBuy = new ButtonColumn(table, editTriggerPoints, 0);
        buttonColumnBuy.setMnemonic(KeyEvent.VK_D);
     
        table.setBackground(new Color(240, 240, 240));


        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        
        return table;
        
        
    }

    
    @Override
    public void update() {
        //redraw the stock panel
        redraw();
        
    }
}