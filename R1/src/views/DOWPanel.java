package views;

import observer.Observer;

import javax.swing.JLabel;

import java.awt.FlowLayout;
import java.text.NumberFormat;

public class DOWPanel extends MainPanel implements Observer{

    JLabel lblValue;
    /**
     * Create the panel.
     */
    public DOWPanel() {
        
        pc.register(this);
        setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        
        JLabel lblDow = new JLabel("DOW");
        add(lblDow);
        
        lblValue = new JLabel("$0.0");
        add(lblValue);
        
        JLabel lblUp = new JLabel("Up");
        add(lblUp);
        

    }

    @Override
    public void update() {
        // TODO Auto-generated method stub
        lblValue.setText(NumberFormat.getCurrencyInstance().format(pc.DOWValue));
        
    }

}
