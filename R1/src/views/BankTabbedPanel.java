package views;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import models.Bank;
import models.Holding;
import observer.Observer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class BankTabbedPanel extends MainPanel implements Observer{
    public JPanel centerPanel;
    /**
     * Create the panel.
     */
    public BankTabbedPanel() {
        setLayout(new BorderLayout(0, 0));
        
        centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);
        centerPanel.setLayout(new BorderLayout(0, 0));
        //register to observe portfolio
        pc.register(this);
        init();
        
    }
    
    public void init(){
        cc.openCSV(userCSVFile);

        JTable table = createTable();
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();
    }
    
    public void redraw(){
        JTable table = createTable();
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();
    }
    

    public JTable createTable(){
        final ArrayList<Holding> banks = pc.getBank(user.getPortfolio());
        
        int len = 6; //this is used to determine the length of each array.
                            
        int wid = banks.size();
        
       // System.out.println(wid);
        Object[][] bankTable;
        if (wid== 0){
            bankTable = new String[1][6];
            bankTable[0][0] = "No Bank";
            bankTable[0][1] = "";
            bankTable[0][2] = "";
            bankTable[0][3] = "";
            bankTable[0][4] = "";
            bankTable[0][5] = "";
            
        }else{
            bankTable = new Object[wid][len];
            Bank bank;
            //System.out.println(wid +" "+ len);
            for(int i = 0 ; i < wid; i ++){
                bank = (Bank)banks.get(i);
                bankTable[i][0] = "Edit";
                bankTable[i][1] = bank.getName();
                bankTable[i][2] = bank.getInitPrice();
                bankTable[i][3] = bank.getAccountNumber();
                bankTable[i][4] = bank.getRoutingNumber();
                bankTable[i][5] = bank.getDate();
               
            }
        }
        
        
        int columns = bankTable[0].length;
        
        
        
        String[] columnNames = new String[columns];
        columnNames[0] = "";
        columnNames[1] = "Bank Name";
        columnNames[2] = "Balance";
        columnNames[3] = "Account Number";
        columnNames[4] = "Rounting Number";
        columnNames[5] = "Date Added";
        
        DefaultTableModel model = new DefaultTableModel(bankTable, columnNames);
        
      
        
        JTable table = new JTable(model);

        
        Action editBank = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                //THIS ALSO THE HOLDING INDEX IN HOLDINGS 
                int modelRow = Integer.valueOf(e.getActionCommand());
                Bank bank = (Bank) banks.get(modelRow);

                JOptionPane.showMessageDialog(null, "TODO _ DEPOSIT AND WITHDRAW");
            }
            
        };
        

        
        //Assign action to button
        ButtonColumn buttonColumnBuy = new ButtonColumn(table, editBank, 0);
        buttonColumnBuy.setMnemonic(KeyEvent.VK_D);
     
        
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        
        return table;
    }


    @Override
    public void update() {
        // TODO Auto-generated method stub
        redraw();
        
    }

}
