package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

import models.Holding;
import models.Stock;
import models.User;
import models.WatchedStock;

public class TriggerPointDialog extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField textFieldHighValue;
    private double highValue = 0;
    private JTextField textFieldLowValue;
    private double lowValue = 0;
    private JLabel lblStock;
    private JLabel lblStockPrice;
    //input stock from market
    private Holding stock;
    //final watched which is return
    private WatchedStock watchedStock;
    private String tickerSymbol;
    private double initPrice;
    private static boolean normalStock = true;
    /**
     * Create the dialog.
     */
    public TriggerPointDialog(Stock stock) {
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);

        setBounds(100, 100, 300, 200);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        
        
            TriggerPointDialog.normalStock = true;
            this.stock = stock;
            this.tickerSymbol = stock.getTickerSymbol();
            this.initPrice = stock.getInitPrice();        
       
        init();
    }
    
    /*
     * Create the dialog from watched stock view
     * 
     */
    public TriggerPointDialog(WatchedStock stock){

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);

        setBounds(100, 100, 300, 200);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        
        this.watchedStock = (WatchedStock)stock;
        TriggerPointDialog.normalStock = false;
        this.tickerSymbol = ((WatchedStock)stock).getTickerSymbolReal();
        this.initPrice = ((WatchedStock)stock).getInitPriceReal();
        this.highValue = ((WatchedStock)stock).getHighTriggerPoint();
        this.lowValue = ((WatchedStock)stock).getLowTriggerPoint();
        init();
    
    }
    
    
    public void init(){
        NumberFormat nf  = NumberFormat.getCurrencyInstance();
        
        textFieldHighValue = new JTextField(nf.format(highValue));
        textFieldHighValue.setBounds(88, 39, 130, 26);
        contentPanel.add(textFieldHighValue);
        textFieldHighValue.setColumns(10);
        
        textFieldLowValue = new JTextField(nf.format(lowValue));
        textFieldLowValue.setBounds(88, 84, 130, 26);
        contentPanel.add(textFieldLowValue);
        textFieldLowValue.setColumns(10);
        
        lblStock = new JLabel(this.tickerSymbol);
        lblStock.setBounds(35, 16, 61, 16);
        contentPanel.add(lblStock);
       
        lblStockPrice = new JLabel(nf.format(this.initPrice));
        
        lblStockPrice.setBounds(130, 16, 75, 16);
        contentPanel.add(lblStockPrice);
        
        JLabel lblHighTriggerPoint = new JLabel("High");
        lblHighTriggerPoint.setBounds(42, 44, 68, 16);
        contentPanel.add(lblHighTriggerPoint);
        
        JLabel lblLow = new JLabel("Low");
        lblLow.setBounds(42, 89, 61, 16);
        contentPanel.add(lblLow);
        
        
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                if(this.normalStock == false){
                    JButton btnDelete = new JButton("Delete Stock");
                    btnDelete.setActionCommand("Delete Stock");
                    buttonPane.add(btnDelete);
                    getRootPane().setDefaultButton(btnDelete);
                    btnDelete.addActionListener(new ActionListener(){

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            
                            MainPanel.pc.deleteWatchedHolding(User.getInstance().getPortfolio(), getWatchedStock());
                            close();
                        }
                        
                    });
                }
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
                okButton.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        editStock();
                    }
                    
                });
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // TODO Auto-generated method stub
                        close();
                    }
                    
                });
            }
        }
    }
    
    public void close(){
        this.setVisible(false);
        this.dispose();
    }


    public double getHighValue() {
        return highValue;
    }


    public void setHighValue(double highValue) {
        this.highValue = highValue;
    }


    public double getLowValue() {
        return lowValue;
    }


    public void setLowValue(double lowValue) {
        this.lowValue = lowValue;
    }
    
    public void editStock(){
       
        
            try{
            	String high = textFieldHighValue.getText().replaceAll("\\$", "");
            	String low = textFieldLowValue.getText().replaceAll("\\$", "");
                highValue = Double.parseDouble(high);
                lowValue = Double.parseDouble(low);
                if (normalStock == true){
                    //Instantiate new watchedStock
                      setWatchedStock(new WatchedStock(stock.getTickerSymbol(), stock.getName(),
                              stock.getInitPrice(), stock.getPartOf(), highValue, lowValue));
                      
                      //Add to portfolio
                      MainPanel.pc.addWatchedHolding(User.getInstance().getPortfolio(),getWatchedStock());
                      
                     

                  }else{
                      //update low and high point trigger
                      this.watchedStock.setHighTriggerPoint(highValue);
                      this.watchedStock.setHighTriggerPoint(lowValue);
                      MainPanel.pc.notifyObserver();
                                             
                  }
                  close();
                
            }catch (NumberFormatException error){              
                error.printStackTrace();
                JOptionPane.showMessageDialog(null, "Invalid Input");
                
            }
        }

    


    public WatchedStock getWatchedStock() {
        return watchedStock;
    }


    public void setWatchedStock(WatchedStock watchedStock) {
        this.watchedStock = watchedStock;
    }
}
