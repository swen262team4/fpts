package views;

import controllers.CSVController;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import models.User;

/*
 * This class constructs the View for Account
 */
public class AccountPanel extends MainPanel {

    /**
     * Create the panel.
     */
    public AccountPanel() {
        User u = User.getInstance();
        setLayout(null);

        JButton btnSignOut = new JButton("Sign Out");
        btnSignOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	JFrame frame = new JFrame();
				int result = JOptionPane.showConfirmDialog(frame, "Save profile before logging out?", "There may be unsaved data", JOptionPane.YES_NO_CANCEL_OPTION);
				if (result == JOptionPane.YES_OPTION){
					CSVController csv = new CSVController();
					csv.savePortfolio();
					
					close();
				}else if (result == JOptionPane.NO_OPTION){
					close();
				}
            }
        });
        btnSignOut.setBounds(24, 29, 117, 29);
        add(btnSignOut);

        
        JLabel lblUsername = new JLabel("Username:");
        lblUsername.setBounds(32, 93, 75, 16);
        add(lblUsername);

        JLabel lblNewLabel = new JLabel(u.getID());
        lblNewLabel.setBounds(106, 93, 61, 16);
        add(lblNewLabel);

    }

}
