
package views;

import javax.swing.JPanel;


import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JMenuItem;

import controllers.PortfolioController;
import observer.Observer;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/*
 * This class constructs the View for Porfolio
 */
public class PortfolioPanel extends MainPanel implements Observer{

    //THIS SHOWS CURRENT UPDATE TIME
    JMenu mnUpdatetime;
    
    /**
     * Create the panel.
     */
    public PortfolioPanel() {
        pc.register(this);
        this.setLayout(new BorderLayout(0, 0));
        
    
        JMenuBar menuBar = new JMenuBar();
        this.add(menuBar, BorderLayout.NORTH);
        
        mnUpdatetime = new JMenu("Update ("+PortfolioController.time/1000+" seconds)");
        menuBar.add(mnUpdatetime);
        
        //UPDATE PORTFOLIO
        JMenuItem mntmNow = new JMenuItem("Now");
        mntmNow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pc.updateHoldings(user.getPortfolio());
            }
        });
        mnUpdatetime.add(mntmNow);
        
        JMenuItem mntmOther = new JMenuItem("Other");
        mntmOther.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame("FPTS Update Timer");

                // prompt the user to enter their name
                String input = JOptionPane.showInputDialog(frame, "Update every ___ seconds");
                try{
                    int time = Integer.parseInt(input);
                    pc.changeAutoUpdateTime(time*1000);;
                }catch(NumberFormatException error){
                    error.printStackTrace();
                }

            }
        });
        mnUpdatetime.add(mntmOther);
        
        JMenu mnUndo = new JMenu("Undo/Redo");
        menuBar.add(mnUndo);
        
        JMenuItem undo = new JMenuItem("Undo Last Action");
        mnUndo.add(undo);
        undo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pc.undoTransaction();
            }
        });
        
        JMenuItem redo = new JMenuItem("Redo Last Action");
        mnUndo.add(redo);
        redo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                pc.redoTransaction();
            }
        });
        
        //SAVE THE CURRENT PORTFOLIO
        JButton btnSave = new JButton("Save");
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                
                cc.savePortfolio();
                JOptionPane.showMessageDialog(null, "Portfolio Saved");
            }
        });
        menuBar.add(btnSave);
        
        JPanel panel = new JPanel();
        this.add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout(0, 0));
        
        JPanel portfolioTabbedView = new PortfolioTabbedView();
        panel.add(portfolioTabbedView, BorderLayout.CENTER);
        
        //THIS PANEL IS USED TO SHOW DOW VALUE
        JPanel DOWPanel = new DOWPanel();
        panel.add(DOWPanel, BorderLayout.SOUTH);
                    
        
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub
        mnUpdatetime.setText("Update ("+PortfolioController.time/1000+" seconds)");
       
        
    }
}
