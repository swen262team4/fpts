package views;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JMenuBar;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import models.Holding;
import models.Stock;
import models.User;
import models.WatchedStock;

/*
 * Panell to show the stock market
 */
public class StockMarketPanel extends MainPanel {

    public JPanel centerPanel;
    /**
     * Create the panel.
     */
    public StockMarketPanel() {

        setLayout(new BorderLayout(0, 0));
        JScrollPane scrollPanel = new JScrollPane();
        //This centerPanel holds the table
        
        centerPanel = new JPanel();
        add(scrollPanel, BorderLayout.CENTER);
        centerPanel.setLayout(new BorderLayout(0, 0));
        scrollPanel.getViewport().add(centerPanel);
        
        init();
    }
    
    public void init(){
        
        
        JTable table = createTable();
        //addButton(table);
        
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();
        
    }   
    
        
    /*
    * pass stock to portfolio controller to add to watched list
    */
    public void toWatch(WatchedStock stock){
        this.pc.addWatchedHolding(User.getInstance().getPortfolio(), stock);
    }

    public JTable createTable(){
        
        cc.openCSV("equities.csv");
        final ArrayList<Holding> holdings = cc.getHoldingList();
  
        int len = 0; //this is used to determine the length of each array.
        for (Holding holding : holdings){
            int size = holding.getPartOf().size();
            if (size > len){
                len = holding.getPartOf().size(); //update longest array size
            }
        }
        //now 4 because new button
        len += 5; //this is done because ticker symbol, name, and initValue are added to the partOf list
        
        int wid = holdings.size();
       // System.out.println(wid);
        
        Object[][] stockTable = new Object[wid][len];
        Holding holding;
        for(int i = 0 ; i < wid; i ++){
            holding = holdings.get(i);
            stockTable[i][0] = "Buy";
            stockTable[i][1] = "Watch";
            stockTable[i][2] = holding.getTickerSymbol();
            stockTable[i][3] = holding.getName();
            stockTable[i][4] = holding.getInitPriceString();
            int j;
            for (j = 0; j < holding.getPartOf().size(); j++){
                stockTable[i][j+5] = holding.getPartOf().get(j);
            }


        }
        int columns = stockTable[0].length;
        

        String[] columnNames = new String[columns];
        columnNames[0] = "Buy";
        columnNames[1] = "Watch";
        columnNames[2] = "Ticker Symbol/Short Name";
        columnNames[3] = "Name";
        columnNames[4] = "Value";
        columnNames[5] = "Additional Information";
        for (int i = 6; i < columns; i++){
            columnNames[i] = "";
        }
        
        DefaultTableModel model = new DefaultTableModel(stockTable, columnNames);
        
      
        
        JTable table = new JTable(model);
        
        //get value of the stock at ith row
        
        /*
         * Define action when Buy button is clicked
         */
        Action buy = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                JTable table = (JTable)e.getSource();
                //THIS ALSO THE HOLDING INDEX IN HOLDINGS parsed from the equities.csv
                int modelRow = Integer.valueOf(e.getActionCommand());
                
                JFrame buyingFrame = new StockTransactionFrame(holdings.get(modelRow));
                buyingFrame.setVisible(true);
                
            }
            
        };
        
        /*
         * Add the stock to the watch list
         */
        Action watch = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub
              //THIS ALSO THE HOLDING INDEX IN HOLDINGS parsed from the equities.csv
                int modelRow = Integer.valueOf(e.getActionCommand());
                Stock stock = ((Stock)holdings.get(modelRow));
                
                //TriggerPointDialog will handle adding the stock
                new TriggerPointDialog(stock);

            }
            
        };
        
        //Assign action to button
        ButtonColumn buttonColumnBuy = new ButtonColumn(table, buy, 0);
        buttonColumnBuy.setMnemonic(KeyEvent.VK_D);
        ButtonColumn buttonColumnWatch = new ButtonColumn(table, watch, 1);
        buttonColumnWatch.setMnemonic(KeyEvent.VK_D);
        
        table.setBackground(new Color(240, 240, 240));


        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        
        return table;
        
        
    }
}
