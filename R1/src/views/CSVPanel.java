package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import controllers.CSVController;
//import sun.awt.VerticalBagLayout;


/*
 * This class constructs the View for CSV Utility 
 */
public class CSVPanel extends MainPanel {


    private static CSVController controller;
    /**
     * Create the panel.
     */
    public CSVPanel() {
        
        this.setLayout(new BorderLayout(0,0));

        this.controller = new CSVController();
        controller.init();
        final JFileChooser fc = new JFileChooser();

        final JScrollPane scrollPanel = new JScrollPane();
        //This centerPanel holds the table
        final JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout(0,0));
        
        final JPanel tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout(0,0));
        
        final JPanel table2Panel = new JPanel();
        table2Panel.setLayout(new BorderLayout(0,0));
        
        //Make a menu
        JMenuBar menuBar = new JMenuBar();
        menuBar.setOpaque(true);
        menuBar.setBackground(new Color(255, 255, 255));
        menuBar.setPreferredSize(new Dimension(1000, 30));


        JMenu export = new JMenu("Export");
        menuBar.add(export);

        final JMenuItem exportItem = new JMenuItem("Export to...",KeyEvent.VK_1);
        export.add(exportItem);
        exportItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                int returnVal = fc.showOpenDialog(exportItem);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    controller.createCSV(fc.getSelectedFile().getAbsolutePath());
                    tablePanel.removeAll();
                    table2Panel.removeAll();
                    JTable table = createTable(controller);
                    table.setMinimumSize(new Dimension(100, 200));
                    
                    JTable table2 = createTransactionTable(controller);
                    table2.setMinimumSize(new Dimension(100, 200));
                    
                    tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
                    tablePanel.add(table, BorderLayout.CENTER);
                    tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
                    tablePanel.revalidate();
                    tablePanel.repaint();
                    table2Panel.add(table2.getTableHeader(), BorderLayout.NORTH);
                    table2Panel.add(table2, BorderLayout.CENTER);
                    table2Panel.revalidate();
                    table2Panel.repaint();
                    centerPanel.revalidate();
                    centerPanel.repaint();
                }
            }
        });


        JMenu importMenu = new JMenu("Import");
        menuBar.add(importMenu);

        final JMenuItem importNewItem = new JMenuItem("Import From...",KeyEvent.VK_1);
        importMenu.add(importNewItem);
        importNewItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                int returnVal = fc.showOpenDialog(importNewItem);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    controller.openCSV(fc.getSelectedFile().getAbsolutePath());
                    tablePanel.removeAll();
                    table2Panel.removeAll();
                    JTable table = createTable(controller);
                    table.setMinimumSize(new Dimension(100, 200));
                    
                    JTable table2 = createTransactionTable(controller);
                    table2.setMinimumSize(new Dimension(100, 200));
                    
                    tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
                    tablePanel.add(table, BorderLayout.CENTER);
                    tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
                    tablePanel.revalidate();
                    tablePanel.repaint();
                    table2Panel.add(table2.getTableHeader(), BorderLayout.NORTH);
                    table2Panel.add(table2, BorderLayout.CENTER);
                    table2Panel.revalidate();
                    table2Panel.repaint();
                    centerPanel.revalidate();
                    centerPanel.repaint();
                }
            }
        });

        final JMenuItem importUserItem = new JMenuItem("Import Portfolio",KeyEvent.VK_2);
        importMenu.add(importUserItem);
        importUserItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                controller.getPortfolio();
                tablePanel.removeAll();
                table2Panel.removeAll();
                JTable table = createTable(controller);
                table.setMinimumSize(new Dimension(100, 200));
                
                JTable table2 = createTransactionTable(controller);
                table2.setMinimumSize(new Dimension(100, 200));
                
                tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
                tablePanel.add(table, BorderLayout.CENTER);
                tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
                tablePanel.revalidate();
                tablePanel.repaint();
                table2Panel.add(table2.getTableHeader(), BorderLayout.NORTH);
                table2Panel.add(table2, BorderLayout.CENTER);
                table2Panel.revalidate();
                table2Panel.repaint();
                centerPanel.revalidate();
                centerPanel.repaint();
            }
        });

        //create holdings table for the center of the view.
        JTable table = createTable(controller);
        table.setMinimumSize(new Dimension(100, 200));
        
        JTable table2 = createTransactionTable(controller);
        table2.setMinimumSize(new Dimension(100, 200));
        
        tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
        tablePanel.add(table, BorderLayout.CENTER);
        tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
        table2Panel.add(table2.getTableHeader(), BorderLayout.NORTH);
        table2Panel.add(table2, BorderLayout.CENTER);
        
        //Set the menu bar and add the label to the content pane.
        add(menuBar, BorderLayout.NORTH);
        
        add(scrollPanel, BorderLayout.CENTER);
        scrollPanel.getViewport().add(centerPanel);
        centerPanel.add(tablePanel, BorderLayout.CENTER);
        centerPanel.add(table2Panel, BorderLayout.SOUTH);
    }

    public JTable createTable(CSVController controller){
        String[][] tableObject = controller.getTable();
        
        
        int columns = tableObject[0].length;
        String[] columnNames = new String[columns];
        columnNames[0] = "Ticker Symbol/Short Name";
        columnNames[1] = "Name";
        columnNames[2] = "Value";
        columnNames[3] = "Additional Information";
        for (int i = 4; i < columns; i++){
            columnNames[i] = "";
        }
        JTable table = new JTable(tableObject, columnNames);
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        return table;
    }
    
    public JTable createTransactionTable(CSVController controller){
        String[][] tableObject = controller.getTransactionTable();
        //System.out.println(tableObject.length);
        String[] columnNames = new String[6];
        columnNames[0] = "Ticker Symbol";
        columnNames[1] = "Name";
        columnNames[2] = "Cost";
        columnNames[3] = "Shares Purchased";
        columnNames[4] = "Date";
        columnNames[5] = "Bank Used for Transaction";
        
        JTable table = new JTable(tableObject, columnNames);
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        return table;
    }

}
