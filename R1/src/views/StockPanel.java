package views;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import observer.Observer;
import models.User;
import controllers.AccountController;
import controllers.CSVController;
import controllers.PortfolioController;

import java.awt.BorderLayout;


/*
 * STock panel, shows user's owned stocks
 */
public class StockPanel extends MainPanel implements Observer{
    

    JPanel centerPanel;
    

    /**
     * Create the panel.
     */
    public StockPanel() {
        setLayout(new BorderLayout(0, 0));
        
        centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);
        centerPanel.setLayout(new BorderLayout(0, 0));
        //register to observe portfolio
        pc.register(this);
        init();
    }
    
    public void init(){
        
        //userCSVFile from mainPanel
        cc.openCSV(userCSVFile);

        JTable table = createTable(cc);
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();
       
    }
    
    
    public void redraw(){
        JTable table = createTable(cc);
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();

    }
    
    
    public JTable createTable(CSVController controller){
        String[][] tableObject = controller.prepareHoldingsForTable(pc.getOwnedStocks(user.getPortfolio()));
        //System.out.println(tableObject.length);
        int columns = tableObject[0].length;
        String[] columnNames = new String[columns];
        columnNames[0] = "Ticker Symbol/Short Name";
        columnNames[1] = "Name";
        columnNames[2] = "Value";
        columnNames[3] = "Additional Information";
        for (int i = 4; i < columns; i++){
            columnNames[i] = "";
        }
        JTable table = new JTable(tableObject, columnNames);
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        return table;
    }
    
    
    
    @Override
    public void update() {
        //redraw the stock panel
        redraw();
        
    }
}