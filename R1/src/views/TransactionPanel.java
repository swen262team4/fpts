package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import observer.Observer;
import controllers.CSVController;

public class TransactionPanel extends MainPanel implements Observer{

    JPanel centerPanel;
    //private JPanel centerPanel;
    /**
     * Create the panel.
     */
    public TransactionPanel() {
        //observe portfolio
        pc.register(this);
        init();
    }

    public void init(){
        this.setLayout(new BorderLayout(0,0));
        final JScrollPane scrollPanel = new JScrollPane();
        //This centerPanel holds the table
        centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout(0,0));
        final JPanel table2Panel = new JPanel();
        table2Panel.setLayout(new BorderLayout(0,0));
        
        add(scrollPanel, BorderLayout.CENTER);
        scrollPanel.getViewport().add(centerPanel);
        centerPanel.add(table2Panel, BorderLayout.CENTER);


        JTable table2 = createTransactionTable(cc);
        //table2.setMinimumSize(new Dimension(100, 200));
        table2Panel.add(table2.getTableHeader(), BorderLayout.NORTH);
        table2Panel.add(table2, BorderLayout.CENTER);
        
        
    }
    
    public void redraw(){
        JTable table = createTransactionTable(cc);
        centerPanel.removeAll();
        centerPanel.add(table.getTableHeader(), BorderLayout.NORTH);
        centerPanel.add(table, BorderLayout.CENTER);
        centerPanel.revalidate();
        centerPanel.repaint();

    }
    
    public JTable createTransactionTable(CSVController controller){
        String[][] tableObject = controller.prepareTransactionsForTable(user.getTransactionList());
        //System.out.println(tableObject.length);
        String[] columnNames = new String[6];
        columnNames[0] = "Ticker Symbol";
        columnNames[1] = "Name";
        columnNames[2] = "Cost";
        columnNames[3] = "Shares Purchased";
        columnNames[4] = "Date";
        columnNames[5] = "Bank Used for Transaction";
        
        JTable table = new JTable(tableObject, columnNames);
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        return table;
    }

    @Override
    public void update() {
        // TODO Auto-generated method stub
        redraw();
        
    }
}
