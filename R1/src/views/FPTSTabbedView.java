package views;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JOptionPane;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


import controllers.CSVController;


/*
 * This class constructs the main view for the application. The main panel contains
 * a JTabbedPanel, which contains other panels (account view, csv view, portfolio view,
 * and simulator view)
 */
public class FPTSTabbedView {

    private JFrame frame;

    
    /**
     * Create the application.
     */
    public FPTSTabbedView() {
        initialize(); 
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        setFrame(new JFrame());
        getFrame().setVisible(false);
        getFrame().setBounds(100, 100, 800, 600);
        getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        getFrame().addWindowListener((WindowListener) new customClosingListener());
        
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        getFrame().getContentPane().add(tabbedPane, BorderLayout.CENTER);
        
        JPanel portfolioPanel = new PortfolioPanel();
        tabbedPane.addTab("Portfolio", null, portfolioPanel, null);
        
        
        JPanel simulatorPanel = new SimulatorPanel();
        tabbedPane.addTab("Simulator", null, simulatorPanel, null);
        
        JPanel csvPanel = new CSVPanel();
        tabbedPane.addTab("CSV Utility", null, csvPanel, null);
        
        JPanel accountPanel = new AccountPanel();
        tabbedPane.addTab("My Account", null, accountPanel, null);
        accountPanel.setLayout(new BorderLayout(0, 0));
        
    }
    
    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }
}

class customClosingListener extends WindowAdapter {
    public void windowClosing(WindowEvent e) {
    	JFrame frame = (JFrame)e.getSource();
		int result = JOptionPane.showConfirmDialog(frame, "Save profile before exiting?", "There may be unsaved data", JOptionPane.YES_NO_CANCEL_OPTION);
		if (result == JOptionPane.YES_OPTION){
			CSVController csv = new CSVController();
			csv.savePortfolio();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}else if (result == JOptionPane.NO_OPTION){
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
    }
}