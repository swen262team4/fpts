package views;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;




import javax.swing.JPanel;

import controllers.AccountController;
import controllers.CSVController;
import controllers.PortfolioController;
import models.User;

/*
 * 
 */
public class MainPanel extends JPanel {

    public User user = User.getInstance();
    public static AccountController ac = new AccountController();
    public static PortfolioController pc = new PortfolioController();
    public static CSVController cc = new CSVController();
    public String userCSVFile = user.getUserCSV();

    /**
     * Create the panel.
     * @user authenticated user from login screen
     */
    public MainPanel() {
    }
    
    public void close(){
        try {
            restartApplication();
        } catch (IOException | URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
            
    }

    /*
     * http://stackoverflow.com/questions/4159802/how-can-i-restart-a-java-application
     */
    public void restartApplication() throws IOException, URISyntaxException
    {
      final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
      System.out.println(javaBin);
      //final File currentJar = new File(FPTS_Run.class.getProtectionDomain().getCodeSource().getLocation().toURI());

      final File currentJar = new File(FPTS_Run.class.getProtectionDomain().getCodeSource().getLocation().toURI());
      System.out.println(currentJar.getAbsolutePath());
      /* is it a jar file? */
      if(!currentJar.getName().endsWith(".jar"))
        return;

      /* Build command: java -jar application.jar */
      final ArrayList<String> command = new ArrayList<String>();
      command.add(javaBin);
      command.add("-jar");
      command.add(currentJar.getPath());

      final ProcessBuilder builder = new ProcessBuilder(command);
      builder.start();
      System.exit(0);
    }

    
   }
