package views;

import javax.swing.JPanel;

import models.OwnedStock;
import observer.Observer;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JMenu;
import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import controllers.SimulationController;
import controllers.SimulationController.algorithmType;
import controllers.SimulationController.stepSize;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;


/*
 * This class construct the View for Simulator
 */
public class SimulatorPanel extends MainPanel implements Observer {

    SimulationController sc = new SimulationController();
    private JPanel leftPanel;
    private JPanel rightPanel;
    private ArrayList<OwnedStock> simulatedHoldings = pc.getOwnedStocks_v2(user.getPortfolio());
    JLabel lblDuration;
    JLabel lblPercent;
    
    // 0 = no change, 1 = Bull, 2 = Bear
    private algorithmType marketType = algorithmType.BULL;
    private stepSize size = stepSize.DAY;
    private JTextField txtNumberOfSize;
    private JTextField txtPercentAnnum;
    private int index = 0;
    private JTable table;
    private JPanel tablePanel;
    
    private ArrayList<HashMap<String, Double> > simulation; 
    
    private JPanel panel;
    /**
     * Create the panel.
     */
    public SimulatorPanel() {
        setLayout(new BorderLayout(0, 0));
        
        pc.register(this);
        
        panel = new JPanel();
        add(panel, BorderLayout.CENTER);
        pc.register(this);
        init();
        
    }
    
    
    public void init(){
        panel.setLayout(new BorderLayout(0, 0));
        
        tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout(0,0));
        
        ButtonGroup marketGroup =  new ButtonGroup();
        
        //=================================================================
        /*
         * STEP SIZE MENU
         */
        
        ButtonGroup stepSizeGroup =  new ButtonGroup();
        JMenuBar menuBar = new JMenuBar();
        panel.add(menuBar, BorderLayout.NORTH);
        
        
        //MENU BAR FOR SOUTH
        JMenuBar menuBarS = new JMenuBar();
        panel.add(menuBarS, BorderLayout.SOUTH);
        
        //ADD LABELS TO SOUTH MENUBAR
        JLabel lblMarket = new JLabel(marketType.toString());
        menuBarS.add(lblMarket);
        lblDuration = new JLabel(" \t");
        menuBarS.add(lblDuration);
        JLabel lblSize = new JLabel(size.toString());
        menuBarS.add(lblSize);
        lblPercent = new JLabel("\t");
        menuBarS.add(lblPercent);


        JMenu marketMenu = new JMenu("Market");
        menuBar.add(marketMenu);
        
        JRadioButtonMenuItem rdbtnmntmBull = new JRadioButtonMenuItem("Bull");
        rdbtnmntmBull.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                marketType = algorithmType.BULL;
                System.out.println(marketType.toString());
            }
        });
        rdbtnmntmBull.setSelected(true);
        marketGroup.add(rdbtnmntmBull);
        marketMenu.add(rdbtnmntmBull);
        
        //BUTTON TO SET BEAR ALGORTHIM
        JRadioButtonMenuItem rdbtnmntmBear = new JRadioButtonMenuItem("Bear");
        rdbtnmntmBear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                marketType = algorithmType.BEAR;
                System.out.println(marketType.toString());
            }
        });
        marketGroup.add(rdbtnmntmBear);
        marketMenu.add(rdbtnmntmBear);
        
        
        //BUTTON TO SET NO CHANGE ALGORITHM
        JRadioButtonMenuItem rdbtnmntmNoChange = new JRadioButtonMenuItem("No Change");
        rdbtnmntmNoChange.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                marketType = algorithmType.NO_CHANGE;
                System.out.println(marketType.toString());
            }
        });
        marketGroup.add(rdbtnmntmNoChange);
        marketMenu.add(rdbtnmntmNoChange);
        
        JMenu mnDuration = new JMenu("Duration");
        menuBar.add(mnDuration);
        
        txtNumberOfSize = new JTextField();
        txtNumberOfSize.setText("1");
        mnDuration.add(txtNumberOfSize);
        txtNumberOfSize.setColumns(10);
        JMenu stepSizeMenu = new JMenu("Size");
        menuBar.add(stepSizeMenu);
        
        JRadioButtonMenuItem rdbtnmntmDay = new JRadioButtonMenuItem("Day");
        rdbtnmntmDay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                size = stepSize.DAY;
            }
        });
        rdbtnmntmDay.setSelected(true);
        stepSizeGroup.add(rdbtnmntmDay);
        stepSizeMenu.add(rdbtnmntmDay);
        
        
        JRadioButtonMenuItem rdbtnmntmWeek = new JRadioButtonMenuItem("Week");
        rdbtnmntmBear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                size = stepSize.WEEK;
            }
        });
        stepSizeGroup.add(rdbtnmntmWeek);
        stepSizeMenu.add(rdbtnmntmWeek);
        
        JRadioButtonMenuItem rdbtnmntmMonth = new JRadioButtonMenuItem("Month");
        rdbtnmntmMonth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                size = stepSize.MONTH;
            }
        });
        stepSizeGroup.add(rdbtnmntmMonth);
        stepSizeMenu.add(rdbtnmntmMonth);
        
        JRadioButtonMenuItem rdbtnmntmYear = new JRadioButtonMenuItem("Year");
        rdbtnmntmMonth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                size = stepSize.YEAR;
            }
        });
        stepSizeGroup.add(rdbtnmntmYear);
        stepSizeMenu.add(rdbtnmntmYear);
        
        JMenu mnPercentAnnum = new JMenu("Percent Anum");
        menuBar.add(mnPercentAnnum);
        
        txtPercentAnnum = new JTextField();
        txtPercentAnnum.setText("100");
        mnPercentAnnum.add(txtPercentAnnum);
        txtPercentAnnum.setColumns(10);
        
        JButton btnSim = new JButton("Simulate Starting Here");
        menuBar.add(btnSim);
        
        btnSim.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SimulationController sc = new SimulationController();
                sc.setDurration(Integer.parseInt(txtNumberOfSize.getText()));
                sc.setPercentAnnum(Double.parseDouble(txtPercentAnnum.getText()));
                sc.setSize(size);
                sc.setType(marketType);
            	simulation = sc.getSimulation(index);
                redraw();
            }
        });
        
        
        //RUN BUTTON
        JButton btnRun = new JButton("Run");
        menuBar.add(btnRun);
        
        btnRun.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                index = Integer.parseInt(txtNumberOfSize.getText());
                redraw();
            }
        });
        
        JButton btnStep = new JButton("Step");
        menuBar.add(btnStep);
        
        btnStep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                index++;
                if (index > Integer.parseInt(txtNumberOfSize.getText())){
                	index = Integer.parseInt(txtNumberOfSize.getText());
                }
                redraw();
            }
        });
        
        JButton btnReset = new JButton("Reset");
        menuBar.add(btnReset);
        
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                index = 0;
                redraw();
            }
        });
        
                
        //PANEL below benubar
        JPanel panel_content = new JPanel();
        panel.add(panel_content, BorderLayout.CENTER);
        panel_content.setLayout(new BorderLayout(0, 0));
        
        panel_content.add(tablePanel, BorderLayout.CENTER);
        table = createTable();
        table.setMinimumSize(new Dimension(100, 200));
        
        tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
        tablePanel.add(table, BorderLayout.CENTER);
        tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
    }

    public JTable createTable(){
    	SimulationController sc = new SimulationController();
        String[][] tableObject = sc.prepareSimulationForTable(simulation, index);
        
        
        int columns = tableObject[0].length;
        String[] columnNames = new String[columns];
        columnNames[0] = "Ticker Symbol/Short Name";
        columnNames[1] = "Name";
        columnNames[2] = "Value";
        columnNames[3] = "Additional Information";
        for (int i = 4; i < columns; i++){
            columnNames[i] = "";
        }
        table = new JTable(tableObject, columnNames);
        table.setBackground(new Color(240, 240, 240));

        table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );

        //Resize column method
        //Code imported from https://tips4java.wordpress.com/2008/11/10/table-column-adjuster/
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int preferredWidth = tableColumn.getMinWidth();
            int maxWidth = tableColumn.getMaxWidth();

            for (int row = 0; row < table.getRowCount(); row++)
            {
                TableCellRenderer cellRenderer = table.getCellRenderer(row, column);
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + table.getIntercellSpacing().width;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check other rows

                if (preferredWidth >= maxWidth)
                {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth( preferredWidth );
        }//end resize column method
        return table;
    }

    @Override
    public void update() {
        redraw();
    }
    
    public void redraw(){
    	tablePanel.removeAll();
    	table = createTable();
      
    	tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
    	tablePanel.add(table, BorderLayout.CENTER);
    	tablePanel.add(new JSeparator(JSeparator.HORIZONTAL), BorderLayout.SOUTH);
    	tablePanel.revalidate();
    	tablePanel.repaint();
    }
    
}
