import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import models.Holding;
import models.Stock;

public class TestREST {
	static final double DOW_DIVISOR = 0.14967727343149;
	public static double DOW_Value = 0;
    protected String getString(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();

            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }

        return null;
    }

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        run();
    }
    
    public static void run() throws IOException, ParserConfigurationException, SAXException{

        String url = "http://query.yahooapis.com/v1/public/yql?q=select%20Symbol,%20LastTradePriceOnly%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22AAPL%22,%22AXP%22,%22BA%22,%22CAT%22,%22CSCO%22,%22CVX%22,%22DD%22,%22DIS%22,%22GE%22,%22GS%22,%22HD%22,%22IBM%22,%22INTC%22,%22JNJ%22,%22JPM%22,%22KO%22,%22MCD%22,%22MMM%22,%22MRK%22,%22MSFT%22,%22NKE%22,%22PFE%22,%22PG%22,%22TRV%22,%22UNH%22,%22UTX%22,%22V%22,%22VZ%22,%22WMT%22,%22XOM%22)&env=store://datatables.org/alltableswithkeys";        
        // Create a URL and open a connection
        String baseUrl ="https://query.yahooapis.com/v1/public/yql";
        String selectLastPriceOnlyUrl ="?q=select%20symbol%2C%20LastTradePriceOnly%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(";
        String doubleQuotes = "%22";
        String comma ="%2C";
        String endUrl =")&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        
        //Dow Jones Industrial Average (^DJI) 
        ArrayList<String> DJI = new ArrayList<String>();
        
        DJI.add("AAPL");
        DJI.add("AXP");
        DJI.add("BA");
        DJI.add("CAT");
        DJI.add("CSCO");
        DJI.add("CVX");
        DJI.add("DD");
        DJI.add("DIS");
        DJI.add("GE");
        DJI.add("GS");
        DJI.add("HD");
        DJI.add("IBM");
        DJI.add("INTC");
        DJI.add("JNJ");
        DJI.add("JPM");
        DJI.add("KO");
        DJI.add("MCD");
        DJI.add("MMM");
        DJI.add("MRK");
        DJI.add("MSFT");
        DJI.add("NKE");
        DJI.add("PFE");
        DJI.add("PG");
        DJI.add("TRV");
        DJI.add("UNH");
        DJI.add("UTX");
        DJI.add("V");
        DJI.add("VZ");
        DJI.add("WMT");
        DJI.add("XOM");
        
        String DJIComponentsUrl ="";
        for (int i = 0; i< DJI.size(); i++){
            if(i == DJI.size()-1){
                //no comma after last index
                DJIComponentsUrl += doubleQuotes+DJI.get(i)+doubleQuotes;
            }else{
                DJIComponentsUrl += doubleQuotes+DJI.get(i)+doubleQuotes+comma;
            }
        }
        
        //final url
        url = baseUrl + selectLastPriceOnlyUrl + DJIComponentsUrl + endUrl;
        
        //System.out.println(url);
        
        URL YahooURL;
        
        YahooURL = new URL(url);

        HttpURLConnection con = (HttpURLConnection) YahooURL.openConnection();

        // Set the HTTP Request type method to GET (Default: GET)
        con.setRequestMethod("GET");
        con.setConnectTimeout(10000);
        con.setReadTimeout(10000);

        // Created a BufferedReader to read the contents of the request.
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {

            response.append(inputLine);
            
        }

        // MAKE SURE TO CLOSE YOUR CONNECTION!
        in.close();
       // xlmWriter.close();

        // response is the contents of the XML
        //System.out.println(response.toString());
        
        
        
        //http://www.javacodegeeks.com/2013/05/parsing-xml-using-dom-sax-and-stax-parser-in-java.html
        //Modified to suit our needs
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        //Load and parse the XML doc
        //Document document = builder.parse(xlmFile);
        Document document = builder.parse(new InputSource(new StringReader(response.toString())));
        //This will return <query> </query>
        Element rootElement = document.getDocumentElement();
        
        ArrayList<Holding> empList = new ArrayList<Holding>();
        
        //Return the results element, which contains stock nodes
        NodeList nodeList = rootElement.getElementsByTagName("results");
        for(int i = 0; i< nodeList.getLength(); i++){
            Node resultNode = nodeList.item(i);
            //return all the quotes
            NodeList resultList = resultNode.getChildNodes();
            for(int j = 0; j <resultList.getLength(); j++){
                
                //encounter <quote symbol="tickerSymbol">
                Node node = resultList.item(j);

                if (node instanceof Element){
                    
                    //String symbol = node.getAttributes().getNamedItem("symbol").getNodeValue();
                	
                    Stock stock = new Stock();
                    //System.out.println(stock.getTickerSymbol());
                    NodeList childNodes = node.getChildNodes();
                    for(int k = 0; k<childNodes.getLength(); k++){
                        Node cNode = childNodes.item(k);
                        //System.out.println(cNode.getNodeName() + " " + cNode.getTextContent());
                        
                        //identifying the child tag
                        
                        if (cNode instanceof Element){
                            String content = cNode.getTextContent().trim();
                            switch(cNode.getNodeName()){
                            case "LastTradePriceOnly" :
                                stock.setInitPrice(Double.parseDouble(content));
                                break;
                            case "Symbol" : 
                            	stock.setTickerSymbol(content);
                            	break;
                            }
                        }
                    }//for stock child nodes
                    empList.add(stock);
                }//iterate <quote> node list
            }// iterate <result> node list
        }//Result element
        double total = 0.0;
        //double average = 0;
        /*
        for(Holding s : empList){
            total += s.getInitPrice();
            System.out.println(s.getTickerSymbol() + " : " + s.getInitPrice());
        }*/
        DOW_Value = total / DOW_DIVISOR; 
        
    }

}
