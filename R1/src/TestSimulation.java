import java.util.ArrayList;
import java.util.HashMap;

import controllers.SimulationController;
import models.Bank;
import models.Holding;
import models.MoneyMarket;
import models.OwnedStock;
import models.Stock;
import models.Transaction;
import models.User;

public class TestSimulation {
	public static void main(String[] args) {
		User user = User.getInstance();
		
		Bank bank1 = new Bank("Bank of America", 87.63, 123456789, 987654321, "20151010");
        Bank bank2 = new Bank("Bank of JOHN CENA", 1000000000.00, 133333337, 133333337, "20020220");
        
        MoneyMarket mm1 = new MoneyMarket("MONEY MONEY MONEY, Inc.", 12.34, 1234, 1234, "20000102");
        MoneyMarket mm2 = new MoneyMarket("Mo` Money Mo` Problems", 43.21, 4321, 4321, "20000102");
        
        ArrayList<String> partOf1 = new ArrayList<String>();
        partOf1.add("DOW");
        OwnedStock stock1 = new OwnedStock("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", partOf1);
        
        ArrayList<String> partOf2 = new ArrayList<String>();
        partOf2.add("NasDaq");
        partOf2.add("OTHER");
        OwnedStock stock2 = new OwnedStock("LIST", "Lists for Listing Lists of Lists & Co.", 34.56, 55.55, "20151015", partOf2);
        
        ArrayList<String> partOf3 = new ArrayList<String>();
        partOf3.add("dow");
        Stock stock3 = new Stock("HELP", "Churches of America and Southern Canada", 34.56, partOf3);
        
        ArrayList<String> partOf4 = new ArrayList<String>();
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        partOf4.add("test");
        Stock stock4 = new Stock("TTT", "Trouble in Terrorist Town, a Mod for Source.", 34.56, partOf4);
        
        Transaction t1 = new Transaction("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", "Bank of America");
        Transaction t2 = new Transaction("ASST", "Assistants, Inc.", 34.56, 55.55, "20151015", "Bank of JOHN CENA");
        
        user.addTransaction(t1);
        user.addTransaction(t2);
        user.getPortfolio().addHolding(bank1);
        user.getPortfolio().addHolding(bank2);
        user.getPortfolio().addHolding(mm1);
        user.getPortfolio().addHolding(mm2);
        user.getPortfolio().addHolding(stock1);
        user.getPortfolio().addHolding(stock2);
        user.getPortfolio().addHolding(stock3);
        user.getPortfolio().addHolding(stock4);
		
        SimulationController controller = new SimulationController(); 
        
        controller.setDurration(52);
        ArrayList<OwnedStock> stocks = new ArrayList<OwnedStock>();
        for (Holding holding : user.getPortfolio().getHoldings()){
        	if (holding.getTickerSymbol() == "!OWNED"){
        		stocks.add((OwnedStock)holding);
        	}
        }
        controller.setHoldings(stocks);
        controller.setPercentAnnum(100);
        controller.setSize(controllers.SimulationController.stepSize.WEEK);
        controller.setType(controllers.SimulationController.algorithmType.BULL);
        controller.simulate();
        
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (HashMap<String, Double> map : controller.getSimulation()){
        	sb.append("Time:" + i + " ");
        	for (OwnedStock stock : stocks){
        		sb.append(map.get(stock.getTickerSymbolReal()) + " ");
        	}
        	i++;
        	sb.append("\n");
        }
        System.out.print(sb);
	}
}