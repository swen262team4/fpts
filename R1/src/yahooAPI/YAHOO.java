package yahooAPI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import models.Holding;
import models.Stock;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class YAHOO {

    public YAHOO(){
        
    }
    
    public HashMap<String,Double> getPrice(String url) throws IOException, ParserConfigurationException, SAXException{

        URL YahooURL = new URL(url);
        HttpURLConnection con = (HttpURLConnection) YahooURL.openConnection();

        // Set the HTTP Request type method to GET (Default: GET)
        con.setRequestMethod("GET");
        con.setConnectTimeout(10000);
        con.setReadTimeout(10000);

        // Created a BufferedReader to read the contents of the request.
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
           
            response.append(inputLine);
            
        }

        // MAKE SURE TO CLOSE YOUR CONNECTION!
        in.close();

        
        
        //http://www.javacodegeeks.com/2013/05/parsing-xml-using-dom-sax-and-stax-parser-in-java.html
        //Modified to suit our needs
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        
        //Load and parse the XML doc
        //Document document = builder.parse(xlmFile);
        Document document = builder.parse(new InputSource(new StringReader(response.toString())));
        //This will return <query> </query>
        Element rootElement = document.getDocumentElement();
        
        HashMap<String,Double> priceMap = new HashMap<String,Double>();
        
        
        ArrayList<Holding> empList = new ArrayList<Holding>();
        
        //Return the results element, which contains stock nodes
        NodeList nodeList = rootElement.getElementsByTagName("results");
        for(int i = 0; i< nodeList.getLength(); i++){
            Node resultNode = nodeList.item(i);
            //return all the quotes
            NodeList resultList = resultNode.getChildNodes();
            for(int j = 0; j <resultList.getLength(); j++){
                
                //encounter <quote symbol="tickerSymbol">
                Node node = resultList.item(j);

                if (node instanceof Element){
                    
                    String symbol = node.getAttributes().getNamedItem("symbol").getNodeValue();
                    
                    Stock stock = new Stock();
                    //System.out.println(stock.getTickerSymbol());
                    NodeList childNodes = node.getChildNodes();
                    for(int k = 0; k<childNodes.getLength(); k++){
                        Node cNode = childNodes.item(k);
                        //System.out.println(cNode.getNodeName() + " " + cNode.getTextContent());
                        priceMap.put(symbol, Double.parseDouble(cNode.getTextContent()));
                        

                    }//for stock child nodes
                    empList.add(stock);
                }//iterate <quote> node list
            }// iterate <result> node list
        }//Result element
        
        
        return priceMap;
        
        
    }
    
    
    
    /*
     * Return url selectLastPriceOnly for a single stock
     */
    String buildURL(Holding stock){
        String url ="";
        String baseUrl ="https://query.yahooapis.com/v1/public/yql";
        String selectLastPriceOnlyUrl ="?q=select%20LastTradePriceOnly%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(";
        String doubleQuotes = "%22";
        String comma ="%2C";
        String endUrl =")&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        String stockComponentsUrl = doubleQuotes + stock.getTickerSymbol() + doubleQuotes;
        
        url = baseUrl + selectLastPriceOnlyUrl + stockComponentsUrl + endUrl;

        return url;
    }
    
    
    public String buildURL(ArrayList<String> tickerList){
        String url ="";
        String baseUrl ="https://query.yahooapis.com/v1/public/yql";
        String selectLastPriceOnlyUrl ="?q=select%20symbol%2C%20LastTradePriceOnly%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(";
        String doubleQuotes = "%22";
        String comma ="%2C";
        String endUrl =")&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        
                
        
        String stockComponentsUrl ="";
        
        for (int i = 0; i< tickerList.size(); i++){
            if(i == tickerList.size()-1){
                //no comma after last index
                stockComponentsUrl += doubleQuotes+tickerList.get(i)+doubleQuotes;
            }else{
                stockComponentsUrl += doubleQuotes+tickerList.get(i)+doubleQuotes+comma;
            }
        }
        
        //final url
        url = baseUrl + selectLastPriceOnlyUrl + stockComponentsUrl + endUrl;
        //System.out.println(url);
        

        return url;
                
    }
    
    public double getDOW() {
        //Dow Jones Industrial Average (^DJI) 
        ArrayList<String> DJI = new ArrayList<String>();
        
        DJI.add("AAPL");
        DJI.add("AXP");
        DJI.add("BA");
        DJI.add("CAT");
        DJI.add("CSCO");
        DJI.add("CVX");
        DJI.add("DD");
        DJI.add("DIS");
        DJI.add("GE");
        DJI.add("GS");
        DJI.add("HD");
        DJI.add("IBM");
        DJI.add("INTC");
        DJI.add("JNJ");
        DJI.add("JPM");
        DJI.add("KO");
        DJI.add("MCD");
        DJI.add("MMM");
        DJI.add("MRK");
        DJI.add("MSFT");
        DJI.add("NKE");
        DJI.add("PFE");
        DJI.add("PG");
        DJI.add("TRV");
        DJI.add("UNH");
        DJI.add("UTX");
        DJI.add("V");
        DJI.add("VZ");
        DJI.add("WMT");
        DJI.add("XOM");
        
        String url = buildURL(DJI);
        HashMap<String,Double> DOWMap = new HashMap<String,Double>();
        try {
            DOWMap = getPrice(url);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        double sum = 0;
        for(double d : DOWMap.values()){
            sum += d;
        }
        double DOWDivisor = 0.14967727343149;
        double DOWValue = sum/DOWDivisor;
        return DOWValue;
        
    }
}
